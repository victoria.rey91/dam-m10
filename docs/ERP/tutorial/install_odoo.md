# Instal·lar Oddo via Docker

## Instal.lar Docker 
Esborrar versions velles:

```
sudo apt-get remove docker docker-engine docker.io containerd runc
```

Actualitzar repositoris:

```
sudo apt update
```

Instal·lar docker

```
sudo apt  install -y docker-ce docker-ce-cli containerd.io
```

Comprovar que funciona:

```
sudo docker run hello-world
```

Comprovar el servei

```
systemctl status docker
```

[How to install docker](https://docs.docker.com/engine/install/debian/#install-using-the-repository)

Instal·lar PIP si no hi és:

```
sudo apt-get install pip
```

## Instal·lar compose
   
És la eina que permet connectar el contenidor de postgres amb de l’Odoo. → [How to install pip](https://docs.docker.com/compose/install/#install-using-pip)

```
sudo pip install docker-compose
```

Crear fitxer ```docker-compose.yml``` 

- [x] No es poden fer servir tabuladors
- [x] Necessita indentar amb espais en blanc, si no dona errors quan l'executem al punt següent.
- [x] Dintre es defineixen els dos contenidors i és mapejen els ports del contenidor docker al de l’aplicació que conté.

!!! note "docker-compose.yaml de la instal·lació d'Odoo"

   ```
   version: '3.1'
   services:
     odoo_p6:
       container_name: odoo_p6
       image: odoo:14.0
       depends_on:
         - mydb_p6
       ports:
         - "8069:8069"
       environment:
         - HOST=mydb_p6
         - USER=odoo
         - PASSWORD=myodoo
       volumes:
         - ./addons:/mnt/extra-addons
     mydb_p6:
       container_name: mydb_p6
       image: postgres:15
       environment:
         - POSTGRES_DB=postgres
         - POSTGRES_PASSWORD=myodoo
         - POSTGRES_USER=odoo
       volumes:
         - ./postgresql:/var/lib/postgresql/data
 
   ```

## Executar fitxer

Per instal·lar el docker, des de la carpeta on es trobi el ```docker-compose.yml``` i executem la comanda:

   ```
   docker-compose up -d
   ```

Obrir navegador i escriure: [http://localhost:8069](http://localhost:8069)

[Comandes de docker](https://chachocool.com/como-instalar-docker-en-debian-10-buster/)

