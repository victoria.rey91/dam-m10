# Principis bàsics amb docker

## Què és docker?

- [x] Docker és una plataforma de programari que permet crear, provar i implementar aplicacions ràpidament.
- [x] Docker utilitza unes unitats estàndard anomenades contenidors, que inclouen tot el necessari per executar el programari.
- [x] Els contenidors són semblants a les màquines virtuals, no obstant no requereixen virtualitzar el kernel del sistema operatiu ni emular maquinari, únicament virtualitzen l’essencial.

## Instal·lem docker?

Per tal d’instal·lar docker a linux podem fer-ho des del terminal. Cal instal·lar uns paquets pre-requisits i afegir el repositori de docker al sistema.

```
sudo apt-get install ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Realitzem novament un:

```
sudo apt-get update
```

I ja simplement hem d’instal·lar els següents paquets:

```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

També multiplataforma

Per tal d’instal·lar-lo a windows o mac, simplement caldrà descarregar el programari [docker desktop](https://www.docker.com/).

*També està disponible per linux.*

Un cop instal·lat ja podrem fer-lo servir, bé amb el programari GUI o bé a terminal.

## Comprovació

Un cop instal·lat realitzem la comprovació de versió per asegurar-nos que tot ha anat correctament: 

```
sudo docker -v
```

En cas de voler evitar fer sudo cada cop, és necessari afegir el nostre usuari al grup docker: 

```
sudo usermod -aG docker $USER
```

Per llençar el nostre primer contenidor, cridarem al clàssic hola mundo: 
```
sudo docker run hello-world
```

Ens retornarà una cosa semblant a açò:

```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete
Digest: sha256:94ebc7edf3401f299cd3376a1669bc0a49aef92d6d2669005f9bc5ef028dc333
Status: Downloaded newer image for hello-world:latest
Hello from Docker!
```

Però que ha passat?

```
Unable to find image 'hello-world:latest' locally
```

El dimoni de docker no ha trobat la imatge de ```hello-world:latest``` al pc. Aquest latest fa referència a la versió, al no indicar-ne cap utilitza l’última disponible.

```
latest: Pulling from library/hello-world Comença la descàrrega del contenidor.
```

Finalitza la descàrrega i comprova amb un checksum que tot ha anat bé.

```
Hello from Docker!
```

Executa el contingut del contenidor.

I ara on està?

Podem comprovar tots els contenidors que tenim engegats amb la comanda: 

```
docker ps
```

Però que passa amb hello-world? El contenidor que acabem d’executar és d’una sola acció i acaba. Podem veure els contenidors existents i aturats amb:

```
docker ps -a

| CONTAINER ID IMAGE       | COMMAND  | CREATED        | STATUS                    | PORTS | NAMES         |
|--------------------------|----------|----------------|---------------------------|-------|---------------|
| ef9191063f97 hello-world | "/hello" | 10 minutes ago | Exited (0) 10 minutes ago |       | busy_brattain | 
```

Per tal d’eliminar hello-world i netejar, realitzarem la comanda: 
```
docker rm $ID
```

LDAP amb docker

Per tal d’instal·lar un servidor l’LDAP amb docker podem fer referència a la guia de [computingforgeeks.com](https://computingforgeeks.com/run-openldap-server-in-docker-containers/), que fa referència al [dockerhub d’osixia](https://github.com/osixia/docker-openldap).

```
docker run --name openldap osixia/openldap:latest
```

I ja està, tenim un servidor d’LDAP funcionant amb docker a la nostra màquina sense cap problema.

## Problemes vs. Solucions

Trobarem diversos problemes amb aquesta execució:

| Problemes                 | Solucions                   |
|---------------------------|-----------------------------|
| Bloqueig del terminal     | -- detach                   |
| Persistència de les dades | Volums o referència al disc |
| Aïllament                 | Exposició de ports          |
| Administració             | Variables d’entorn          |

## Codi al terminal

```
docker run 	--name openldap-server \
-p 389:389 -p 636:636 \
--hostname ldap.computingforgeeks.com \
--env LDAP_ORGANISATION="My Company" \
--env LDAP_DOMAIN="computingforgeeks.com" \
--env LDAP_ADMIN_PASSWORD="StrongAdminPassw0rd" \
--env LDAP_BASE_DN="dc=computingforgeeks,dc=com" \
--volume /data/slapd/database:/var/lib/ldap \
--volume /data/slapd/config:/etc/ldap/slapd.d \
--detach osixia/openldap:latest
```

## Pas a Pas

| Pas a pas                                                                                                                                                                                                                    | Explicació                                                                                                                                                                                                     |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ```docker run```<br/>```--name openldap-server```                                                                                                                                                                            | Amb l’opció --name podem indicar un nom específic per al nostre contenidor. No és essencial, però ens ajudarà a localitzar-lo més endavant.                                                                    |
| ```-p 389:389 -p 636:636```                                                                                                                                                                                                  | Amb l’opció -p podem exposar un port. Si ens fixem observarem que hi ha dos números separats per “:”. Aquests fan referència, el primer al port assignat al host i el segon al port que escolta el contenidor. |
| ```--hostname ldap.computingforgeeks.com```                                                                                                                                                                                  | Amb l’opció --hostname podem indicar un nom específic per al nostre “host”. Al final el contenidor actuarà com una màquina i aquesta tindrà un nom a la xarxa.                                                 |
| ```--env LDAP_ORGANISATION="My Company" \```<br/>```--env LDAP_DOMAIN="computingforgeeks.com" \```<br/>```--env LDAP_ADMIN_PASSWORD="StrongAdminPassw0rd" \```<br/>```--env LDAP_BASE_DN="dc=computingforgeeks,dc=com" \ ``` | Amb l’opció --env podem indicar variables d’entorn. Aquestes variables ens ajudaran a configurar diverses dades que el programari que “instal·larem” necessita per funcionar.                                  |
| ``` --volume /data/slapd/database:/var/lib/ldap \```<br/>```--volume /data/slapd/config:/etc/ldap/slapd.d \```                                                                                                               | Amb l’opció --volume podem fer persistents les dades del contenidor assignant-los un “lloc” al disc local. En cas de no fer-ho, en aturar-lo es perdrà tot el que s’ha fet.                                    |
| ```--detach```                                                                                                                                                                                                               | Amb l’opció --detach ens permetrà que el contenidor s’execute en segon pla, deixant lliure el terminal per seguir treballant.                                                                                  |

Comandes per treballar amb docker

| Comandes                                                                                                                                                                               | Explicació                                                                                                                                                                                                     |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ```docker ps```<br/>```docker ps -a ```                                                                                                                                                | Docker ps ens permetrà veure els dockers que hi ha engegats al host en aquell moment. Amb l’opció -a (all) els veurem tots. Trobarem informació diversa com el nom, ID, els ports exposats, temps en execució… |
| ```docker start NOM_CONTAINER```<br/>```docker stop NOM_CONTAINER```<br/>```docker restart NOM_CONTAINER```                                                                            | Docker start, stop o restart, ens permetrà engegar, aturar o reiniciar el contenidor que li indiquem.                                                                                                          |
| ```docker volumes ls```                                                                                                                                                                | Docker volumes ens permetrà veure els volums que existeixen. Sols veurem si hem fet referència a algun contenidor un volum virtual. Si hem referenciat un directori directe del disc no sortirà.               |
| ```docker images```                                                                                                                                                                    | Docker images ens permet veure les imatges que tenim descarregades al host, aquelles que cerca el dimoni quan executa un docker run.                                                                           |
| ```docker rm ID_CONTENIDOR```                                                                                                                                                          | Docker rm ens permetrà eliminar un contenidor. És necessari que el contenidor estigui aturat per poder esborrar-lo.                                                                                            |
| ```docker exec [OPTIONS] CONTENIDOR COMANDA [ARG...]```                                                                                                                                | Docker exec ens permet executar comandes dintre del contenidor.                                                                                                                                                |
| ```docker exec -it mycontainer sh```                                                                                                                                                   | Aquesta ordre en concret ens permetrà obrir un terminal sh com si estiguéssim al contenidor.                                                                                                                   |