## Enumera breument els diferents motius pels que pot arribar a fallar un projecte d'implantació d'un ERP.
Riscos d'implantació d'un ERP

- Elecció inadequada del software a instal·lar
- Pobre selecció de l’empresa instal·ladora
- Manca de formació a tots els nivells i usuaris del sistema
- Manca d’implicació per part de la direcció
- Fricció en l’ús del sistema ERP per part dels usuaris
- Costos ocults
- Pobre financiació del projecte
- Manca de seguiment post-implantació

## Cita els 5 dels mòduls més importants d'un ERP per una cadena de venda de material informàtic

1. Finances
2. Compres
3. Recursos Humans
4. Vendes i distribució
5. Màrqueting i vendes
   
Mòduls funcionals d’un ERP

- [x] Un ERP està format per diferents mòduls
- [x] Cada mòdul està destinat a una àrea d'una empresa
- [x] Treballen de forma integrada, la informació és compartida en temps real entre tots els mòduls.

| Mòduls generals més comuns |                                                                                   | 
|----------------------------|-----------------------------------------------------------------------------------|
| - Vendes                   | - Gestió documental                                                               |
| - Compres                  | - Fabricació                                                                      |
| - Gestió d'estocs          | - TPV (Terminal punto de venta)                                                   |
| - Gestió de tresoreria     | - CRM (Customer Relationship Management. Gestió de la relació amb el client)      |
| - Comptabilitat            | - MRP (Material Resource Planning relaciona Compres, Vendes, Stock, Producció...) |

| Altres mòduls, *que amplien els mòduls bàsics i no són d’obligada instal·lació/compra* |                                  | 
|----------------------------------------------------------------------------------------|----------------------------------|
| - Gestió documental (DMS)                                                              | - Contractació i recursos humans |
| - Tasques de reporting i intel·ligència de negoci (BI)                                 | - Anàlisi de competidors         |

| Mòduls específics *==(mòduls verticals)== per cada tipologia concreta de negoci* | 
|----------------------------------------------------------------------------------|
| - Gestió de reparacions                                                          |
| - Terminal punt de venda                                                         |
| - Traçabilitat                                                                   |

Aquests mòduls, a la vegada, es poden classificar en:

| Backoffice: sostenen processos interns de l’empresa. No tenen contacte directe amb clients/proveïdors. | Frontoffice: tracten processos externs a l’empresa |
|--------------------------------------------------------------------------------------------------------|----------------------------------------------------|
| - Gestió de personal                                                                                   | - Gestió de clients                                |
| - Processos comptables                                                                                 | - Gestió de proveïdors                             |
|                                                                                                        | - Bancs                                            |

## Indica tres beneficis o característiques dels sistemes documentals

1. Digitalización de documentos
2. Localización central
3. Mejorar el flujo de trabajo
4. Seguridad de la información
5. Compartir documentos
6. Colaboración documental
7. Control de versiones

## Cita almenys  4 dels inconvenients (riscos )de la implantació d'un ERP

!!! danger "Riscos d'un sistema ERP"

    - [ ] És un sistema complex
    - [ ] Implementacions llargues, si es fa a mida
    - [ ] Estructura jeràrquica i centralitza la informació
    - [ ] **Costos alts**: llicències o manteniments
    - [ ] Impacte sobre els usuaris i la corba d'aprenentatge
    - [ ] Integracions externes complicades
    - [ ] L’adaptació dels processos interns 


## Explica breument què és un CRM.

CRM (Customer Relationship Management): Gestió de la relació amb els clients.
Està orientat normalment a gestionar tres àrees bàsiques:

- [x] La gestió comercial
- [x] El màrqueting
- [x] El servei-postvenda o d'atenció al client

Dona suport a una estratègia orientada al client en la qual totes les accions tenen l'objectiu ﬁnal de millorar
l’atenció i les relacions amb clients i clients potencials.

El CRM està inclòs en un ERP, però empreses que no necessiten o no tenen ERP, poden treure molts beneficis
de la implantació d’un CRM.


## Enumera els diferents tipus d'instal·lacions (implantació) d'ERPs i en què consisteix cadascun.

- Implantació modular o per fases (stepbystep)
- Implantació global o big bang

## Explica breument què és un ERP.

- [x] **ERP** *(Enterprise Resource Planning)*: Gestió operativa dels processos de la companyia.
- És un conjunt d'aplicacions relacionades amb els processos de negoci d'una empresa, que permet assolir els objectius estratègics definits pels directius d'una organització

## Enumera al menys 4 criteris a tenir en compte a l'hora d'escollir un ERP.

 Criteris de Selecció

|                          |                                                                                                             |                            |                                                                                                            |
|--------------------------|-------------------------------------------------------------------------------------------------------------|----------------------------|------------------------------------------------------------------------------------------------------------|
| **Característiques**     | Funcionalitats a cobrir <br/>Integració amb BI<br/> Nivell de parametrització                               | **Seguretat de les dades** | Bloqueig a usuaris no autoritzats<br/>Bloqueig entrada dades incorrectes<br/>Traçabilitat de modificacions |
| **Facilitat d’ús**       | Simple<br/>Eficaç<br/>Eficient<br/>Intuitiu<br/>Ergonòmic                                                   | **Internacionalització**   | Adaptació a diferents països<br/>Adaptació a diferents idiomes                                             |
| **Sostenibilitat**       | Del Software<br/>De l’Implantador<br/>De la Plataforma                                                      | **Preu**                   |                                                                                                            |
| **Informació de Gestió** | Ajuda a la presa de decisions (BI)<br/>Integritat de la base de dades<br/>Publicació i recuperació de dades |                            |                                                                                                            |

## Cita almenys 3 avantatges dels ERPs que recordis

- Control activitat departaments de l’empresa
- Millora el processos: temps, costos i productivitat
- Millora la gestió de l’inventari
- Facilita la incorporació del comerç electrònic
- Tancament financer menys complicats

!!! success "Beneficis d'un sistema ERP"

    - [x] Optimització dels processos de gestió: temps, costos → productivitat
    - [x] Coherència i homogeneïtat de la informació (un únic arxiu d'articles, un únic arxiu de clients, etc.)
    - [x] Integritat i unicitat del sistema d'informació (dada única)
    - [x] Ús compartit del mateix sistema d'informació que facilita la comunicació interna i externa
    - [x] Reducció dels costos de gestió de la informació (fora Excel, Access, Suites...)
    - [x] Globalització de la formació (bé o malament, però tots igual)
    - [x] Augment de la productivitat (si es fa "bé") i minimitza errors humans
    - [x] Control dels costos, dels terminis de posada en marxa i d'implementació
    - [x] Mobilitat - Treball en remot: mantenir la productivitat i col·laboració
    - [x] Incrementa la satisfacció dels clients
    - [x] Permet prendre millors decisions empresarials. Es disposa de tota la informació en tot moment, en temps real i amb integritat de les dades
    - [x] Tancament financer menys complicat
    - [x] Control activitat dels departaments de l’empresa
    - [x] Millora la gestió de l’inventari
    - [x] Facilita la incorporació del comerç electrònic



## Cita dos dels sistemes ERP propietaris més utilitzats

- SAP
- Oracle
- Microsoft dynamics
- SAGE