# BPM

## Processos de l’Empresa

![img_9.png](../../src/images/procesos-empresa.png)

El BPM (**Business Process Management**) és l'enteniment, visibilitat, modelat i control dels processos de negoci d'una organització, tot això amb la finalitat d'augmentar l'eficiència de l'empresa i la satisfacció del client → Millora contínua.

Avui dia, els programaris BPM no són només una eina per descriure i controlar els processos, sinó que comencen a incorporar:

- Suport per a la col·laboració humana en temps real (intra i inter processos), inclosa la integració amb les xarxes socials, l'accés mòbil i en el núvol als processos.
- Anàlisi avançat, monitoratge d'activitats en temps real i administració de decisions per a la coordinació intel·ligent i la gestió de les interaccions dels participants del procés.

## Exemples

![img_10.png](../../src/images/process-validació1.png)

![img_11.png](../../src/images/process-validació2.png)
