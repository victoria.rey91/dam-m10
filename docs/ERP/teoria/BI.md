# BI

Conjunt de metodologies, aplicacions i tecnologies que permeten reunir, depurar i transformar dades dels sistemes transaccionals i informació desestructurada (interna i externa a la companyia) en informació estructurada, per a la seva explotació directa (reporting, anàlisi, alertes ...) o per la seva anàlisi i presa de decisions sobre el negoci.

Genera un potencial avantatge competitiu: proporcionar informació privilegiada per respondre als problemes de negoci: entrada a nous mercats, promocions o ofertes de productes, eliminació d'illes d'informació, control financer, optimització de costos, planificació de la producció, anàlisi de perfils de clients, rendibilitat d'un producte concret, etc ...

- Dona una visió de conjunt del negoci
- Millora els processos de presa de decisions
- Facilita l'accés i la difusió d'informació 
- Permet anàlisi de dades en temps real

<figure markdown>
  ![Sail container](../../src/images/BI.png){ width="500" }
  <figcaption>Utilitats de BI</figcaption>
</figure>

Big Data
<iframe width="560" height="315" src="https://www.youtube.com/embed/w4vsFKMO7XA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

[Abrir](https://youtu.be/w4vsFKMO7XA)

BI

<iframe width="560" height="315" src="https://www.youtube.com/embed/ElgUy_7eYLQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

[Abrir](https://youtu.be/ElgUy_7eYLQ)