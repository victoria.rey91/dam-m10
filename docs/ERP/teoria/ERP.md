# ERP – Sistema de planificació de recursos empresarials

És un conjunt d'aplicacions relacionades amb els processos de negoci d'una empresa, que permet assolir els objectius estratègics definits pels directius d'una organització.
## ERP

Un ERP és un tipus de solució de gestió empresarial tant per a pimes com per a grans companyies.
Ajuda a "optimitzar" les seves tasques, facilitant així la seva expansió. Integra en un únic sistema tots els processos de negocis de l’empresa.
Les àrees i departaments que poden beneficiar de l'ús d'un ERP és molt àmplia.

<figure markdown>
  ![Sail container](../../src/images/ERP-image.png){ width="500" }
  <figcaption>ERP</figcaption>
</figure>

### Passat
| Pre-historia                                                                                                | Suites                                          |
|-------------------------------------------------------------------------------------------------------------|-------------------------------------------------|
| Gestió de la informació amb fulls de càlcul varis<br/> ![Sail container](../../src/images/pre-historia.png) | ![Sail container](../../src/images/suites.png)  |

## Què s’espera d’un ERP?
1. Millorar el funcionament de l'organització.
2. Assegurar l’acompliment de la llei i norma comptable.
3. Facilitar la feina als empleats.
4. Integrar millor els sistemes entre les diferents localitzacions de l’empresa.
5. Millorar l’atenció cap als clients. 6.	…

[En resum, millorar el ROI, fent la feina més fàcil, més ràpida i millor.](https://papelesdeinteligencia.com/que-es-un-erp/)

## Principals característiques
- [x] **Modularitat**: implantació per mòduls o funcionalitats
- [x] **Integració**: integra els diferents departaments de l’empresa
- [x] **Adaptabilitat**: Altament configurable

<figure markdown>
  ![Sail container](../../src/images/ERP-moduls.png){ width="500" }
  <figcaption>Móduls d'un ERP</figcaption>
</figure>

Ha de permetre:

- La producció (si l'organització incorpora processos productius)
- La gestió completa dels circuits de compra-venda (logística, distribució, inventari i facturació)
- La gestió financera.
- Gestió de recursos humans.
- Gestió de CRM (gestió de la relació amb els clients)
- Anàlisi i avaluació de l'estat del negoci

## Mòduls funcionals d’un ERP

- [x] Un ERP està format per diferents mòduls
- [x] Cada mòdul està destinat a una àrea d'una empresa
- [x] Treballen de forma integrada, la informació és compartida en temps real entre tots els mòduls.

| Mòduls generals més comuns |                                                                                   | 
|----------------------------|-----------------------------------------------------------------------------------|
| - Vendes                   | - Gestió documental                                                               |
| - Compres                  | - Fabricació                                                                      |
| - Gestió d'estocs          | - TPV (Terminal punto de venta)                                                   |
| - Gestió de tresoreria     | - CRM (Customer Relationship Management. Gestió de la relació amb el client)      |
| - Comptabilitat            | - MRP (Material Resource Planning relaciona Compres, Vendes, Stock, Producció...) |

| Altres mòduls, *que amplien els mòduls bàsics i no són d’obligada instal·lació/compra* |                                  | 
|----------------------------------------------------------------------------------------|----------------------------------|
| - Gestió documental (DMS)                                                              | - Contractació i recursos humans |
| - Tasques de reporting i intel·ligència de negoci (BI)                                 | - Anàlisi de competidors         |

| Mòduls específics *==(mòduls verticals)== per cada tipologia concreta de negoci* | 
|----------------------------------------------------------------------------------|
| - Gestió de reparacions                                                          |
| - Terminal punt de venda                                                         |
 | - Traçabilitat                                                                   |

Aquests mòduls, a la vegada, es poden classificar en:

| Backoffice: sostenen processos interns de l’empresa. No tenen contacte directe amb clients/proveïdors. | Frontoffice: tracten processos externs a l’empresa |
|--------------------------------------------------------------------------------------------------------|----------------------------------------------------|
 | - Gestió de personal                                                                                   | - Gestió de clients                                |
 | - Processos comptables                                                                                 | - Gestió de proveïdors                             |
 |                                                                                                        | - Bancs                                            |

## Funcionalitats

<figure markdown>
  ![Sail container](../../src/images/software-features.png){ width="500" }
</figure>

[Top requested ERP Software Features](https://www.softwareadvice.com/erp/#top-products)

!!! success "Beneficis d'un sistema ERP"

    - [x] Optimització dels processos de gestió: temps, costos → productivitat
    - [x] Coherència i homogeneïtat de la informació (un únic arxiu d'articles, un únic arxiu de clients, etc.)
    - [x] Integritat i unicitat del sistema d'informació (dada única)
    - [x] Ús compartit del mateix sistema d'informació que facilita la comunicació interna i externa
    - [x] Reducció dels costos de gestió de la informació (fora Excel, Access, Suites...)
    - [x] Globalització de la formació (bé o malament, però tots igual)
    - [x] Augment de la productivitat (si es fa "bé") i minimitza errors humans
    - [x] Control dels costos, dels terminis de posada en marxa i d'implementació
    - [x] Mobilitat - Treball en remot: mantenir la productivitat i col·laboració
    - [x] Incrementa la satisfacció dels clients
    - [x] Permet prendre millors decisions empresarials. Es disposa de tota la informació en tot moment, en temps real i amb integritat de les dades
    - [x] Tancament financer menys complicat
    - [x] Control activitat dels departaments de l’empresa
    - [x] Millora la gestió de l’inventari
    - [x] Facilita la incorporació del comerç electrònic

!!! danger "Riscos d'un sistema ERP"

    - [ ] És un sistema complex
    - [ ] Implementacions llargues, si es fa a mida
    - [ ] Estructura jeràrquica i centralitza la informació
    - [ ] **Costos alts**: llicències o manteniments
    - [ ] Impacte sobre els usuaris i la corba d'aprenentatge
    - [ ] Integracions externes complicades
    - [ ] L’adaptació dels processos interns 


## ERP exemple Mòduls SAP

![img_3.png](../../src/images/SAP-Example.png)

## Criteris de Selecció
|                          |                                                                                                             |                            |                                                                                                            |
|--------------------------|-------------------------------------------------------------------------------------------------------------|----------------------------|------------------------------------------------------------------------------------------------------------|
| **Característiques**     | Funcionalitats a cobrir <br/>Integració amb BI<br/> Nivell de parametrització                               | **Seguretat de les dades** | Bloqueig a usuaris no autoritzats<br/>Bloqueig entrada dades incorrectes<br/>Traçabilitat de modificacions |
| **Facilitat d’ús**       | Simple<br/>Eficaç<br/>Eficient<br/>Intuitiu<br/>Ergonòmic                                                   | **Internacionalització**   | Adaptació a diferents països<br/>Adaptació a diferents idiomes                                             |
| **Sostenibilitat**       | Del Software<br/>De l’Implantador<br/>De la Plataforma                                                      | **Preu**                   |                                                                                                            |
| **Informació de Gestió** | Ajuda a la presa de decisions (BI)<br/>Integritat de la base de dades<br/>Publicació i recuperació de dades |                            |                                                                                                            |
<!--* Característiques
    * Funcionalitats a cobrir 
    * Integració amb BI 
    * Nivell de parametrització

* Facilitat d’ús
    * Simple    
    * Eficaç   
    * Eficient
    * Intuitiu
    * Ergonòmic

* Sostenibilitat
    * Del Software
    * De l’Implantador
    * De la Plataforma

* Informació de Gestió
    * Ajuda a la presa de decisions (BI)
    * Integritat de la base de dades
    * Publicació i recuperació de dades

* Seguretat de les dades
    * Bloqueig a usuaris no autoritzats
    * Bloqueig entrada dades incorrectes
    * Traçabilitat de modificacions

* Internacionalització
    * Adaptació a diferents països
    * Adaptació a diferents idiomes

* Preu-->
## Implantació ERP

![img_3.png](../../src/images/Implantació.png)

- [x] Implantació modular o per fases (step by step)
- [x] Implantació global o big bang

## Models d’instal·lació
- **Especialització**: vertical vs. horitzontal
- **Instal·lació**: local vs. al núvol
- **Codi emprat**: codi obert vs. propietari
- **Desenvolupament**: desenvolupament a mida vs. codi propietari

### Especialització

| ERP Veritcal                                                                       | ERP Horizontal                                                                                                           |
|------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| Un ERP vertical normalment està orientat o desenvolupat per a un sector específic. | Les solucions horitzontals per contra no estan especialitzades i serveixen, en principi, per a qualsevol mena d'empresa. |

!!! abstract "ERP Verticals"
    
    L'avantatge d'aquests sistemes és que són solucions específiques per a un tipus de negoci en concret i estan preparades per cobrir només les necessitats d'aquestes companyies. Per tant, el seu desplegament és molt més ràpid.

!!! example "Exemples ERP verticals"

    - [x] **Prophet21**: és una solució per a grans magatzems.
    - [x] **JustFoodERP**: és un paquet específic per a empreses de restauració.
    - [x] **Epicor Distribution**: elaborat per a empreses de distribució.
    - [x] **Skubana**: una solució dissenyada per a grans empreses de retail
    - [x] **Infor ERP Automotriz**: fabricants de vehicles o peces automotrius
    - [x] **ODOO**: Sector neteja

### Desenvolupament

Desenvolupament a mida:

- Estalvi de costos de llicències
- Més cost en desenvolupament, formació, documentació…
- Desenvolupament i, per tant, implantació lenta
- Ajusta les necessitats de l’empresa
- Difícil resposta als canvis 

[Exemple](https://luisvilanova.es/desarrollo-erp-vs-desarrollo-a-medida-en-el-ano-2014/)

### Codi emprat
<!--ERP propietari vs. Open Source-->
| ERP propietari                                       | ERP OpenSource                                                        |
|------------------------------------------------------|-----------------------------------------------------------------------|
| Software existent. Paquetitzat en mòduls             | No hi ha cost de llicències                                           | 
| Cost de llicència alt                                | Podem accedir al codi font. Desenvolupament a mida de les necessitats |
| Implantació ràpida (respecte desenvolupament a mida) | Implantació ràpida i/o esglaonada                                     |
 | Suport, manteniment, documentació, formació          | No hi ha suport/manteniment. Sí forums i comunitat.                   |
 | L’empresa s’adapta al producte                       |                                                                       |

### Instal·lació
#### Arquitectura
Aplicacions client servidor (usuaris, aplicació i dades)

1. Descentralitzada 
2. Escalable
3. Oberta
4. Multi plataforma

#### On-premise vs. cloud
Tradicionalment, les aplicacions ERP/CRM/BI han estat allotjades a les instal·lacions de les organitzacions compradores de les llicències de l'aplicació, desplegament **on-premise**. Però això està canviant.

!!! info

    En aquests moments s’imposen els models **cloud** amb diversos models de desplegament/monetització (IaaS, PaaS i SaaS) que conviuen amb el model tradicional on-premise.

[Referencia](https://www.itjungle.com/2018/04/04/erp-caught-up-in-cloud-updrafts/)

| ERP on premise (instal·lació en local en servidors de l'empresa)                                                                                                                                                                                     | ERP futur (paquet ofimàtic empresarial)                                                                                                                                                                                            |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Normalment es necessita almenys un **recurs IT** dedicat a l'empresa per a una efectiva implantació ERP, donat les integracions amb altres software necessàries.                                                                                     | **Més ràpides d'implantar**. Els recursos interns necessaris també es redueixen.                                                                                                                                                   |
| Les solucions on premise solen oferir **més nivell de personalització** que les solucions online, encara que això està canviant últimament.                                                                                                          | Les **dades** estan en **servidors aliens** (públics o privats, però no els controlem).                                                                                                                                            |
| El **llicenciament està basat en la compra de la solució** i, depenent del proveïdor, en el pagament anual d'un manteniment i suport. Aquesta opció també ofereix més flexibilitat en la configuració de l'ERP en conjunt amb el proveïdor de l'ERP. | Les solucions en el núvol solen oferir-se en manera subscripció, només **es paga per les llicències i usuaris de manera mensual o anual. Inversió inicial menor** i major flexibilitat a l'hora de modificar usuaris i llicències. |

[Comparativa](https://www.softwareadvice.com/resources/cloud-erp-vs-on-premis21e/)

#### Models de Serveis Cloud
<figure markdown>
  ![Sail container](../../src/images/serveis-cloud-1.png){ width="500" }
</figure>

| Exemple amb app                                     | Exemple figurat                                     |
|-----------------------------------------------------|-----------------------------------------------------|
| ![img_15.png](../../src/images/serveis-cloud-2.png) | ![img_16.png](../../src/images/serveis-cloud-3.png) |

- [x] **Infrastructure as a Service (IaaS)**: es contracta únicament les infraestructures tecnològiques (capacitat de procés, d'emmagatzematge i/o de comunicacions) sobre les quals instal·la les seves plataformes (sistemes operatius) i aplicacions. **L'usuari té el control total sobre les plataformes i aplicacions, però no té cap control sobre les infraestructures.**
- [x] **Platform as a Service (PaaS)**: es contracta un servei que permet allotjar i desenvolupar aplicacions (siguin desenvolupaments propis o llicències adquirides) en una plataforma que disposa d'eines de desenvolupament per tal que l'usuari pugui elaborar una solució; en aquest model, el proveïdor ofereix l'ús de la seva plataforma que a la vegada es troba allotjada en infraestructures, de la seva propietat o d'altri. **L'usuari no té cap control sobre la plataforma ni sobre la infraestructura, però manté el control total sobre les seves aplicacions.**
- [x] **Software as a Service (SaaS)**: es contracta la utilització d'unes determinades aplicacions sobre les quals únicament pot exercir accions de configuració i parametrització permeses pel proveïdor. **L'usuari no té cap control sobre l'aplicació, la plataforma i la infraestructura.**

![img_5.png](../../src/images/compare-cloud-on-premise1.png)

[Comparació Cloud-On premise](https://www.datadec.es/blog/erp-cloud-erp-on-premise-erp-saas-erp-comprado)

![img_6.png](../../src/images/compare-cloud-on-premise2.png)

[Una altra comparativa online vs on premise](https://dvit.me/en/odoo-erp-online.html)

#### Riscos d'implantació d'un ERP

- Elecció inadequada del software a instal·lar
- Pobre selecció de l’empresa instal·ladora 
- Manca de formació a tots els nivells i usuaris del sistema
- Manca d’implicació per part de la direcció
- Fricció en l’ús del sistema ERP per part dels usuaris
- Costos ocults
- Pobre financiació del projecte
- Manca de seguiment post-implantació

#### Models de llicència i preus

![img_7.png](../../src/images/compare-llicencia-preus.png)

#### Costos on premise vs núvol

<figure markdown>
  ![Sail container](../../src/images/costes.png){ width="500" }
</figure>

<!-- ![img_8.png](../../src/images/costes.png)-->

## ERP Actuals
<figure markdown>
  ![Sail container](../../src/images/ERP-Actuals.png){ width="500" }
  <figcaption>Comparativa d'ERP a l'actualitat</figcaption>
</figure>

## ERP Futur
| ERP Actual                                                                                                                        | ERP futur (paquet ofimàtic empresarial)                                                                                                |
|-----------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|
| Eficiència <br/> Eficàcia i interpretació dins i entre tots els component rellevants (consumidors, empreses... ) a escala mundial | Eficiència<br/>Eficàcia i integració dels nous serveis de comerç mòbils                                                                | 
| Tota l'organització s'estén a altres organitzacions (socis, clients, proveïdors...) Comerç electrònic                             | Tota l'organització i els seus components de la cadena de valor, i també altres organitzacions de comerç mòbil                         |
| Internet<br/>Arquitectura<br/>Serveis web<br/>Xarxa sense fils<br/>Gestió de coneixement                                          | Internet<br/>Arquitectura més oberta<br/>Xarxa sense fil<br/>Dispositius mòbils<br/>Intel·ligència artificial<br/>Informàtica en núvol |

## Proveïdors

!!! example "Programari propietari i Lliure"

    | Programari propietari | Programari lliure |
    |-----------------------|-------------------|
    | SAP                   | Openbravo         | 
    | Oracle                | Adampiere         |
    | Microsoft             | Open ERP          |

<figure markdown>
  ![Sail container](../../src/images/Proveidors.png){ width="500" }
  <figcaption>Comparativa de proveïdors de productes ERP</figcaption>
</figure>

## Comparativa
### Top 5 ERP més utilitzats

<figure markdown>
  ![img_9.png](../../src/images/top5-1.png){width="500" }
</figure>

### Top 5 millors ERP segons els experts

<figure markdown>
  ![img_10.png](../../src/images/top5-2.png){width="500" }
</figure>

### Top 5 ERP millor valorats pels usuaris

<figure markdown>
  ![img_11.png](../../src/images/top5-3.png){width="500" }
</figure>

### Top 5 ERP més econòmics

<figure markdown>
  ![img_12.png](../../src/images/top5-4.png){width="500" }
</figure>



## Referencies

[Qué es un ERP: Guía completa para elegir el mejor ERP de 2022](https://papelesdeinteligencia.com/que-es-un-erp/)

[Cuales son los programas ERP mas usados del mundo en 2019](https://papelesdeinteligencia.com/que-es-un-erp/#Cuales_son_los_programas_ERP_mas_usados_del_mundo_en_2019)

[Tabla comparativa de mejores programas ERP](https://papelesdeinteligencia.com/que-es-un-erp/#Tabla_comparativa_de_mejores_programas_ERP)

[Relacion de los mejores sistemas ERP gratuitos](https://papelesdeinteligencia.com/que-es-un-erp/#Relacion_de_los_mejores_sistemas_ERP_gratuitos)