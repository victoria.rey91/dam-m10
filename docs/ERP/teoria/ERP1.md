Eines de Gestió Empresarial

Un ERP és un tipus de solució de gestió empresarial tant per a pimes com per a grans companyies
Ajuda a "optimitzar" les seves tasques, facilitant així la seva expansió. Integra en un únic sistema tots els processos de negocis de l’empresa
Les àrees i departaments que poden beneficiar de l'ús d'un ERP és molt àmplia

Gestió de la informació amb fulls de càlcul varis
![img.png](../../src/images/pre-historia.png)

Amb “Suites”
![img_1.png](../../src/images/suites.png)

ERP: Beneficis
Optimització dels processos de gestió (fluxos econòmics i financers)
Coherència i homogeneïtat de la informació (un únic arxiu d'articles, un únic arxiu de clients, etc.)
Integritat i unicitat del sistema d'informació (dada única)
Ús compartit del mateix sistema d'informació que facilita la comunicació interna i externa
Reducció dels costos de gestió de la informació (fora Excel, Access, Suites...)
Globalització de la formació (bé o malament, però tots igual)
Augment de la productivitat (si es fa "bé") i minimitza errors humans
Control dels costos, dels terminis de posada en marxa i d'implementació
Mobilitat - Treball en remot: mantenir la productivitat i col·laboració
Incrementa la satisfacció dels clients
Permet prendre millors decisions empresarials. Es disposa de tota la informació en tot moment, en temps real i amb integritat de les dades 6

Els costos d'adquisició del software
La implantació del sistema
La corba d'aprenentatge
L’adaptació dels processos interns

1. Millorar el funcionament de l'organització.
2. Assegurar l’acompliment de la llei i norma comptable.
3. Facilitar la feina als empleats.
4. Integrar millor els sistemes entre les diferents localitzacions de l’empresa.
5. Millorar l’atenció cap als clients. 6. …

En resum, millorar el ROI, fent la feina més fàcil, més ràpida i millor.
https://papelesdeinteligencia.com/que-es-un-erp/

És un sistema integral
És un sistema modular
És un sistema adaptable

Ha de permetre:
- La producció (si l'organització incorpora processos productius)
- La gestió completa dels circuits de compra-venda (logística, distribució, inventari i facturació)
- La gestió financera.
- Gestió de recursos humans.
- Gestió de CRM (gestió de la relació amb els clients)
- Anàlisi i avaluació de l'estat del negoci

Un ERP està format per diferents mòduls
Cada mòdul està destinat a una àrea d'una empresa
Treballen de forma integrada, la informació és compartida en temps real entre tots els mòduls.
Existeixen mòduls de caràcter general (mòduls bàsics), comuns a totes les empreses, com vendes, compres, comptabilitat,
etc.; altres mòduls que són opcionals, que amplien els mòduls bàsics i no són d’obligada instal·lació/compra; i
existeixen mòduls específics (mòduls verticals) per cada tipologia concreta de negoci, com per exemple: gestió de
reparacions, terminal punt de venda, traçabilitat, etc.

Els mòduls anteriors, a la vegada, es poden classificar en:

Backoﬃce: sostenen processos interns de l’empresa. No tenen contacte directe amb clients/proveïdors. P.e: gestió de
personal, processos comptables, ...
Frontoffice: tracten processos externs a l’empresa: gestió de clients, gestió de proveïdors, bancs, ...

Mòduls generals més comuns:
Vendes
Compres
Gestió d'estocs
Gestió de tresoreria
Comptabilitat
Gestió documental
Fabricació
TPV (Terminal punto de venda)
Altres mòduls
Gestió documental (DMS)
Tasques de reporting i intel·ligència de negoci (BI)
Contractació i recursos humans
Anàlisi de competidors
CRM (Customer Relationship Management. Gestió de la relació amb el client)
MRP (Material Resource Planning relaciona Compres, Vendes, Stock,Producció..)

https://www.softwareadvice.com/erp/#top-products

Característiques:
funcionalitats a cobrir
integració amb BI
nivell de parametrització
Facilitat d’ús:
Simple
Eficaç
Eficient
Intuitiu
Ergonòmic
Sostenibilitat:
del Software
de l’Implantador
de la Plataforma
Informació de Gestió:
ajuda a la presa de decisions (BI)
integritat de la base de dades
publicació i recuperació de dades
Seguretat de les dades:
bloqueig a usuaris no autoritzats
bloqueig entrada dades incorrectes
traçabilitat de modificacions
Internacionalització:
adaptació a diferents països
adaptació a diferents idiomes
Preu
15

Especialització: vertical vs. horitzontal
Instal·lació: local vs al núvol
Codi emprat: codi obert vs. propietari
Desenvolupament: desenvolupament a mida vs codi propietari

Un ERP vertical normalment està orientat o desenvolupat per a un sector específic.

L'avantatge d'aquests sistemes és que són solucions específiques per a un tipus de negoci en concret i estan preparades
per cobrir només les necessitats d'aquestes companyies. Per tant, el seu desplegament és molt més ràpid.
Les solucions horitzontals per contra no estan especialitzades i serveixen, en principi, per a qualsevol tipus
d'empresa.

Alguns ERP verticals:
Prophet21: és una solució per a grans magatzems.
JustFoodERP: és un paquet específic per a empreses de restauració.
Epicor Distribution: elaborat per a empreses de distribució.
Skubana: una solució dissenyada per a grans empreses de retail
Infor ERP Automotriz: fabricantes de vehículos o piezas automotrices
ERP ODOO VERTICAL EMPRESAS SECTOR LIMPIEZA 17

Desenvolupament a mida:

Estalvi de costos de llicències
Més cost en desenvolupament, formació, documentació…
Desenvolupament i per tant implantació lenta
Ajusta les necessitats de l’empresa
Difícil resposta als canvis Exemple:
https://luisvilanova.es/desarrollo-erp-vs-desarrollo-a-medida-en-el-ano-2014/

ERP propietari
Software existent. Paquetitzat en mòduls
Cost de llicència alt
Implantació ràpida (respecte desenvolupament a mida)
Suport, manteniment, documentació, formació
L’empresa s’adapta al producte

ERP OpenSource
No hi ha cost de llicències
Podem accedir al codi font. Desenvolupament a mida de les necessitats
Implantació ràpida i/o esglaonada
No hi ha suport/manteniment. Sí forums i comunitat.

Tradicionalment, les aplicacions ERP/CRM/BI han estat allotjades a les instal·lacions de les organitzacions compradores
de les llicències de l'aplicació, desplegament on-premise. Però això està canviant.
En aquests moments s’imposen els models cloud amb diversos models de desplegament/monetització (IaaS, PaaS i SaaS) que
conviuen amb el model tradicional on-premise.

20
https://www.itjungle.com/2018/04/04/erp-caught-up-in-cloud-updrafts/

ERP on premise (instal·lació en local en servidors de l'empresa):
Normalment es necessita almenys un recurs IT dedicat a l'empresa per a una efectiva implantació ERP, donat les
integracions amb altres software necessàries.
Les solucions on premise solen oferir més nivell de personalització que les solucions online, encara que això està
canviant últimament.
El llicenciament està basat en la compra de la solució i, depenent del proveïdor, en el pagament anual d'un manteniment
i suport. Aquesta opció també ofereix més ﬂexibilitat en la configuració de l'ERP en conjunt amb el proveïdor de l'ERP.
ERP cloud (en el núvol):
Més ràpides d'implantar. Els recursos interns necessaris també es redueixen.
Les dades estan en servidors aliens (públics o privats, però no els controlem).
Les solucions en el núvol solen oferir-se en manera subscripció, només es paga per les llicències i usuaris de manera
mensual o anual. Inversió inicial menor i major ﬂexibilitat a l'hora de modificar usuaris i llicències.
https://www.softwareadvice.com/resources/cloud-erp-vs-on-premis21e/

Models de Serveis Cloud

22

Infrastructure as a Service (IaaS): es contracta únicament les infraestructures tecnològiques (capacitat de procés,
d'emmagatzematge i/o de comunicacions) sobre les quals hi instal·la les seves plataformes (sistemes operatius) i
aplicacions. L'usuari té el control total sobre les plataformes i aplicacions, però no té cap control sobre les
infraestructures.
Plataform as a Service (PaaS): es contracta un servei que permet allotjar i desenvolupar aplicacions ( ja siguin
desenvolupaments propis o llicències adquirides) en una plataforma que disposa d'eines de desenvolupament per tal que l'
usuari pugui elaborar una solució; en aquest model, el proveïdor ofereix l'ús de la seva plataforma que a la vegada es
troba allotjada en infraestructures, de la seva propietat o d'altri. L'usuari no té cap control sobre la plataforma ni
sobre la infraestructura però manté el control total sobre les seves aplicacions.
Software as a Service (SaaS): es contracta la utilització d'unes determinades aplicacions sobre les quals únicament pot
exercir accions de configuració i parametrització permeses pel proveïdor.
L'usuari no té cap control sobre l'aplicació, la plataforma i la infraestructura. 23

https://www.datadec.es/blog/erp-cloud-erp-on-premise-erp-saas-erp-comprado    24

Implantació

Una altra comparativa online vs onpremise: https://dvit.me/en/odoo-erp-online.html

Elecció inadequada del software a instal·lar

Pobre selecció de l’empresa instal·ladora

Manca de formació a tots els nivells i usuaris del sistema

Manca d’implicació per part de la direcció

Fricció en l’ús del sistema ERP per part dels usuaris

Costos ocults

Pobre financiació del projecte

Manca de seguiment post-implantació

(Normalment hi ha una versió community però també una versió Enterprise de pagament)

https://www.dolibarr.org/ http://www.openbravo.com/ https://erpnext.com/ http://www.tryton.org/ https://www.odoo.com/

https://blog.capterra.com/free-open-source-erp-software/    29

https://papelesdeinteligencia.com/que-es-un-erp/

Cuáles son los programas ERP mas usados del mundo en 2019

Tabla comparativa de mejores programas ERP

Relación de los mejores sistemas ERP gratuitos

