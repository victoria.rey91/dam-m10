# Eines de Gestió Empresarial

- [x] **ERP** *(Enterprise Resource Planning)*: Gestió operativa dels processos de la companyia.
- [x] **CRM** *(Customer Relationship Management)*: Gestió de la relació amb els clients.
- [x] **ECM** *(Enterprise Content Management)*: Gestió de la documentació, cicle de vida i ﬂux de treball de documents.
- [x] **BI** *(Business Intelligence)*: Informació per a prendre decisions, tant sobre processos operatius com estratègiques.
- [x] **BPM** *(Business Process Management)*: Modelització, implementació i execució d'activitats interrelacionades, és a dir: Processos.
