# CRM: Customer Relationship Management

Està orientat normalment a gestionar tres àrees bàsiques:

- [x] La gestió comercial
- [x] El màrqueting
- [x] El servei-postvenda o d'atenció al client

Dona suport a una estratègia orientada al client en la qual totes les accions tenen l'objectiu ﬁnal de millorar
l’atenció i les relacions amb clients i clients potencials.

El CRM està inclòs en un ERP, però empreses que no necessiten o no tenen ERP, poden treure molts beneficis
de la implantació d’un CRM.

<figure markdown>
  ![Sail container](../../src/images/CRM-Modules.png){ width="500" }
  <figcaption>Departaments que afecten a un CRM</figcaption>
</figure>

## Beneficis
- Augment de l'índex de **fidelització dels clients**. Fidelitzar un client costa 5 vegades menys que l'obtenció d'un nou.
- **Estalvi de temps**, eliminant tasques rutinàries i repetitives. D'aquesta manera poden centrar-se en objectius més estratègics i productius.
- Optimització de la **col·laboració entre els serveis**, des de l'obtenció ﬁns a la investigació de dades valuoses sobre els clients. La informació es difon i la utilitzen totes les àrees de l'empresa.
- Capacitat de resposta. Les dades recollides permeten analitzar els problemes comuns i faciliten una **visió global** de les incidències per avaluar millor les necessitats de clients i fer seguiment de processos interns.
- **Augment dels beneficis de l'empresa**, permetent desenvolupar el valor de la cartera de clients augmentant els marges.

## Criteris de Selecció
- **Possibilitat de col·laboració i comunicació** entre múltiples interlocutors per coordinar les intervencions de diversos participants (comercials, socis, distribuïdors ...).
- **Facilitat de posada en marxa** de la solució: valor que obtindrem vs. esforç que cal realitzar, tenint en compte les competències informàtiques dels treballadors.
- **Facilitat d'integració** amb altres programaris i processos de l'empresa.
- **Preu**

## Implantació

Els punts a tenir en compte de cara a l'elecció del tipus d'implementació són semblants als de l'ERP.

Per definir correctament la solució caldrà:

1. Involucrar els diferents participants.
2. Identificar i avaluar les necessitats dels diversos usuaris (comercials, màrqueting, suport, etc ...).
3. Configurar l'aplicació i definir els nous processos que la utilitzaran (canviarà segur la manera de treballar).
4. Posar en marxa l'aplicació, validar-la i acompanyar per veure si són necessaris ajustos
<figure markdown>
  ![Sail container](../../src/images/Implantació-CRM.png){ width="300" }
  <figcaption>Process d'implantació</figcaption>
</figure>

## Competidors principals
<figure markdown>
  ![Sail container](../../src/images/Competidores-CRM.png){ width="500" }
  <figcaption>Taula comparativa de competidors de CRM</figcaption>
</figure>

https://www.zendesk.com/blog/gartner-magic-quadrant-crm/

