# ECM: 

## Gestor Documental

Durant segles, la gestió documental va ser el domini exclusiu d'administradors, arxivers i bibliotecaris, les eines manuals bàsiques eren els llibres de registre, les carpetes, arxivadors, caixes i prestatgeries en què es guarden els documents de paper (i més tard els audiovisuals i els documents en suports magnètics o òptics, etc ...). 

Més recentment es van anar sumant a ells els mitjans informàtics, que són cada vegada més necessaris a causa de la complexitat de l'activitat administrativa. Tot i que beneficien substancialment la gestió documental.

Un ECM (**Enterprise Content Management**) o sistema de gestió documental assegura una informació organitzada i eficient, que afavoreixi la productivitat empresarial. 

## Beneficis
- Digitalització de documents
- Repositori centralitzat
- Millorar fluxos de treball i optimització de processos (eliminant burocràcia)
- Seguretat 
- Compartir documents 
- Col·laboració 
- Control de versions

<figure markdown>
  ![Sail container](../../src/images/comparadores-ECM.png){ width="500" }
  <figcaption>Taula comparativa de competidors d'ECM</figcaption>
</figure>

https://www.g2.com/categories/enterprise-content-management-ecm?trending=

