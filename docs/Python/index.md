# Introducció a Python3

## Sessió interactiva vs arxius .py
Per entrar a la sessió interactiva:
```
$ python
$ python3
```
Per executar un programa Python:
```
$ python nomfitxer.py
$ python3 nomfitxer.py
```
Per sortir-ne:
```
$ exit()
```
Per executar un programa Python amb sessió interactiva:
```
$ python -i nomfitxer.py
$ python3 -i nomfitxer.py
```
**Python**

- Llenguatge interpretat
- Tipat dinàmic (no cal declarar variables. Es creen el primer cop que s’utilitzen)
- Llenguatge fortament tipat (no admet operacions entre variables de tipus diferents)
- Llenguatge Multiparadigma: admet Orientació a objectes, programació funcional i programació imperativa

## Sintaxi bàsica
- Indentació. No hi ha delimitadors de bloc
- La capçalera de blocs compostos: If, while, def, class ha d’acabar amb “:” (“;” opcional al final)
- Key sensitive
- Comentaris:

```
# una línia
‘’’	o ”””
Comentaris de vàries línies
‘’’	o “””
```

## Variables
- Python té tipat dinàmic. No cal declarar variables!
- La declaració es fa automàticament quan s’assigna un valor a una variable.
- Les variables poden canviar de tipus, simplement assignant-hi un nou valor d'un tipus diferent.

    * x = 1
    * x = “hola”

- Permet assignar un valor únic a diverses variables simultàniament.
    
    * a = b = c = 1

- Permet assignar diversos objectes a diverses variables.

    * a, b, c = 1, 2, “hola”

**Entrada i Sortida**
La funció **print** (diferent a Python2)

La sintaxi completa és:
> print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)
```python
>>> print (“Hola”, ”mundo”) -> no concatena
Hola mundo
>>> print (“Hola” + “mundo”) -> concatena: dóna error si les dades no són del mateix tipus
Holamundo	
>>> edat=22
>>> print ("Té"+ edat + "anys") -> error
>>> print ("Té ",  edat, " anys") -> ok 
???????
```

La funció **input** (diferent a Python2)
```python
>>> pes=input(“Quin és el teu pes? ”)
Quin és el teu pes? 70.5
>>> type(pes)
<class 'str'>
>>> pes=float(input(“Quin és el teu pes? ”))
Quin és el teu pes? 70.5
>>> type(pes)
<class 'float'>
```
Tipus de dades

![img.png](teoria/img.png)

[https://www.w3schools.com/python/python_datatypes.asp](https://www.w3schools.com/python/python_datatypes.asp)