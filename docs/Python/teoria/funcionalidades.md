## Sorting a Python
Hi ha diferents maneres d’ordenar a Python. Podeu veure quina convé més aquí:
[https://docs.python.org/3/howto/sorting.html](https://docs.python.org/3/howto/sorting.html)

## Condicionals
No hi ha case/switch

True/False són objectes de classe bool i són immutables

Qualsevol valor no-zero i no-null és True, altrament False

[https://docs.python.org/3/library/stdtypes.html#truth-value-testing](https://docs.python.org/3/library/stdtypes.html#truth-value-testing)

```python
edad = int(input("¿Cuántos años tiene? ")) 
if edad >= 18: 
    print("Es usted mayor de edad")
elif edad < 0:
    print("No se puede tener una edad negativa")
else:
    print("Es usted menor de edad")
```

Python ofereix una alternativa:
**<expressió1> if <condició> else <expressió2>**

!!! example

    \>\>\> age=13<br/>
    \>\>\> print('kid' if age < 12 else 'teenager')<br/>
    if age < 12:<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;print('kid')<br/>
    else:<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;print('teenager')

## Bucles 

### While i For
**while**
```python
        
edad = 0
while edad<18:
    print(edad)
```

**for**
```python

for letter in ‘Python’:
    print(letter)
fruits = [‘pera’,‘poma’, ‘platan’] 
for fruit in fruits:
    print(fruit)
for index in range(len(fruits)):
    print (fruites[index])
```

### Sentències control flux

Break, continue, pass
```python
for letter in 'hola_mundo':
    if letter == '_':
        pass 	# (/ continue/ break)
    print(letter)
print('\nfinish loop')
```

| Sentència | Descripció                                                  |
|-----------|-------------------------------------------------------------|
| Break     | Surt del bucle                                              |
| Continue  | Surt de la iteració                                         |
| Pass      | Ignora la condició                                          |
| **else**  | El bucle ha acabat normalment (en cas que no faci un break) |

## Funcions
Quan troba return acaba

``` python
def nomfuncio( par1, par2...):
    ["Tip / Explicació"]
    codi de la funció
    [return [expression]][;]

nomfuncio(’a’,’b’)
X = nomfuncio(’a’,’b’)
```

Paràmetres:

- Es poden passar desordenats posant el nom

```   
nomfuncio( par2=’b’, par1=’b’)
```

- Valors per defecte: 

```
def nomfuncio( par1, par2=’b’):
```

- Opcionals:

```
def nomfuncio( par1, par2=None): 
``` 

- Núm de paràmetres variable 

```
def nomfuncio(*pars):
```

### Paràmetres variables
```python
def parametres_variables(param1, *params, **kwords):
    print(param1) 
    for x in params:
        print(x)
    # Els paràmetres kwords es recorren com diccionaris 
    for clau in kwords:
        print("El valor de", clau, "es", kwords[clau])
    
parametres_variables("A", "b", "b", "d", clau1="1", clau2="2")
```

### Paràmetres
Cas invers: la funció espera una sèrie de paràmetres, però els tenim en una llista o diccionari. Ho indiquem amb * o **.
```python
def calcular(preu, iva): 
    return preu + (preu * iva / 100)

imports = [100, 21] 
print calcular(*imports)

imports = {"iva": 15, "preu": 100} 
print calcular(**imports)
```

Recordem que a Python tot són objectes. Els noms que definim són simplement identificadors vinculats a aquests objectes. Les funcions no són excepcions, també són objectes (amb atributs).

- Noms diferents poden fer referència al mateix objecte de funció.
- Es poden passar com a paràmetre a una altra funció.
- Una funció pot retornar una altra funció
- Es poden assignar a variables i guardar en estructures de dades

```python
def first(msg): 
    print(msg)

first("Hello")

second = first 
second("Hello")
```

```python
def is_called(): 
    def is_returned():
        print("Hello") 
    return is_returned 
    
new = is_called() 

#Outputs "Hello"
new()
```
