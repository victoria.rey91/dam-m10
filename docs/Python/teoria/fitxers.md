
## Fitxers

### Obrir

- objFitxer = open(nom_fitxer \[, mode_acces]\[, buffering]) 

mode_acces:

- "r" - Llegir - Valor per defecte. Obre un fitxer per a la lectura, error si el fitxer no existeix
- "a" - Append - Obre un fitxer per afegir, crea el fitxer si no existeix
- "w" - Escriure - Obre un fitxer per escriure (sobreescriu), crea el fitxer si no existeix
- "x" - Crea el fitxer especificat, retorna un error si el fitxer existeix

A més, podeu especificar si el fitxer s'ha de gestionar com a mode binari o de text

- "t" - Text - Valor per defecte. Mode de text
- "b" - Binari - Mode binari

### Tancar

- **objFitxer.close()** _(Fa flush i tanca)_

Llegir:

- **objFitxer.read()**: llegeix tot el fitxer
- **objFitxer.read(n)**: llegeix tot els n primers caràcters 
- **objFitxer.readline()**: llegeix una línia 
- **objFitxer.readlines()**: llegeix totes les línies a una llista

### Escriure

- **objFitxer.write(string)**

### Eliminar

- **objFitxer.remove(nomfixer)**
``` python
import os
if os.path.exists("demofile.txt"):
    os.remove("demofile.txt")
else:
    print("The file does not exist")
```

Per recórrer un fitxer:
```python
f = open("demofile.txt", "r")
for x in f:	#llegeix línia a línia
    print(x)

f = open("demofile.txt", "r") 
print(f.read()) #llegeix tot el fitxer de cop

#És necessari tancar el fitxer: 
f.close()
```

Sentència **with**:
La sentència with tancarà el fitxer. ^^No cal fer close^^
```python
with open(“testfile.txt”) as f:
for line in f: 
    print line

with open(path_fitxer) as f:
    lines = f.readlines()

for line in lines:
    print line
```
[https://www.w3schools.com/python/python_file_handling.asp](https://www.w3schools.com/python/python_file_handling.asp)

## Excepcions
Sintaxi:
```python
try:
    print("Hola") 
except FileNotFoundError:
    print("Fitxer no trobat")
except Exception as e:	#Podem accedir a “e” 
    print("Excepció inesperada") 
    print(e, type(e))
except: 
    print("Error inesperat")  #qualsevol altre tipus d’error
else: 
    print("No hi ha hagut error")	#no s’ha produït cap excepció
finally:
    print("Final") #s’executa sempre, tant si hi ha error com si no
```
[https://docs.python.org/3/tutorial/errors.html](https://docs.python.org/3/tutorial/errors.html)

[https://docs.python.org/3/library/exceptions.html](https://docs.python.org/3/library/exceptions.html)

Llençar una excepció:
```python
x = -1 
if x < 0: 
    raise Exception("Sorry, no numbers below zero")
    
Definir el tipus d’error amb TypeError:
x = "hello" 
if not type(x) is int:
    raise TypeError("Only integers are allowed")
```

## Classes
Sintaxi:
```python
class Person: 
    contador = 0                    #variable de la classe
    def __init__(self, name, age):	#el constructor 
        self.name = name 
        self.age = age
    
    def myfunc(self):	            #mètode de la classe 
        print("Hello my name is " + self.name)
    
una_persona = Person("John", 36)

print(una_persona.name) 
una_persona.myfunc()
```

[https://www.tutorialspoint.com/python/python_classes_objects.ht43m](https://www.tutorialspoint.com/python/python_classes_objects.ht43m)

``` python
class Person: 
    self.name = name 
    self.age = age

    def myfunc(self): 
        print("Hello my name is " + self.name) 

una_persona = Person("John", 36)

print(una_persona.name) 
una_persona.myfunc() 
una_persona.age = 18 
del una_persona.age
del una_persona	
```

Els **mètodes** reben **implícitament** l'objecte sobre el qual s'invoquen.

Poden tenir el nom que es vulguin enlloc de "self", però ha de ser el primer paràmetre.

_S'ha de fer servir **pass** en cas de classes buïda._

!!! abstract

    **del** elimina una propietat de la classe o bé un objecte.

### Herència

```python
class Student(Person, Objecte2):	        #HEREDA DE Person i Objecte2
                                            #Pot heredar de + objectes
    def __init__(self, name, age, cicle):
        #opció 1
        Person.__init__(self, name, age)
        #opció 2
        super().__init__(fname, lname)
        #afegim un atribut 
        self.cicle = cicle
    
    def myfunc(self): 
        print("My name is " + self.name + "i estudio " + self.cicle) 

un_estudiant = Student("John", 36, "DAM")
```

### Mètodes especials (màgic methods):
```python
def __init__(self):     #equival al constructor de Java i inicialitza 
def __del__(self):      #destructor
def __new__(self):      #és el constructor pròpiament
def __add__(self): 
def __sub__(self):      #podem sobrecarregar els operadors + i -
def __cmp__(self):      #comparació
def __str__(self):      #equival al toString() de java
```
[https://rszalski.github.io/magicmethods/](https://rszalski.github.io/magicmethods/)

### Atributs i mètodes privats

```python
class P:
    def __init__(self, name, alias): 
        self.name = name        # public 
        self.__alias = alias    # private

    def __myMethod(self):      # private method 
        print('This is private method')
```
[https://www.bogotobogo.com/python/python_private_attributes_methods.php](https://www.bogotobogo.com/python/python_private_attributes_methods.php)

### Built-in class functions
- **getattr** (obj, name [, default]): per accedir a l'atribut
- **hasattr** (obj, nom): per comprovar si un atribut existeix o no
- **setattr** (obj, nom, valor): per establir un atribut. Si l'atribut no existeix, llavors es crearia.
- **delattr** (obj, name): per eliminar un atribut

Per exemple quan tenim els noms dels atributs en una llista o variable… els llegim d’un fitxer…

[https://docs.python.org/3/library/functions.html](https://docs.python.org/3/library/functions.html)

[https://chase-seibert.github.io/blog/2013/04/12/getattr-setattr.html https://www.w3schools.com/python/python_ref_functions.asp](https://chase-seibert.github.io/blog/2013/04/12/getattr-setattr.html https://www.w3schools.com/python/python_ref_functions.asp)

!!! question "Exercici"

    Fes una classe Gos que tingui:<br/>

    - Un atribut espècie amb valor “gos”<br/>
    - En crear un objecte d’aquesta classe, s’ha d’especificar els atributs:<br/>

        - Nom
        - Edat
        - Raça

[https://realpython.com/python3-object-oriented-programming/](https://realpython.com/python3-object-oriented-programming/)

## Mòduls
Un mòdul és un fitxer de codi Python (entensió .py).

Pot contenir funcions, classes i variables. Per fer servir un mòdul:

```python
import persona (o import persona as p) # tot el mòdul
    persona.Persona(...) (o p.Persona(...))
from persona import Persona #una part del mòdul, p.e. un diccionari
    Persona[edad] 
from persona import * # desaconsellable. S’accedeix sense dot notation 
```
[https://www.programiz.com/python-programming/module50s](https://www.programiz.com/python-programming/module50s)

Quan executem un mòdul s’executa el codi amb nivell d’indentació 0. No hi ha un main().

El main és implícitament el codi de nivell 0.

Si volem que un codi s’executi quan executem el mòdul, però no quan l’importem:
```
if __name__ == '__main__':
```

\_\_main\_\_ és una variable built-in que avalua al nom del mòdul actual si l’hem importat o a main si l’estem executant directament. En aquest cas el codi de nivell 0 seria aquest if, i s’executaria el que hi ha dins. Podem fer:
```
if __name__ == '__main__':
    main()
```

### Alias

```
import modulo as m
import paquete.modulo1 as pm
import paquete.subpaquete.mod1 as psm
```

Per accedir:
```
print m.CONSTANTE_1
print pm.CONSTANTE_1
print psm.CONSTANTE_1
```

**PEP 8**: La importació de mòduls s'ha de fer al començament del document, en ordre alfabètic de paquets i mòduls.

Primer han d'importar els mòduls propis de Python. Després, els mòduls de tercers i finalment, els mòduls propis de l'aplicació.

Entre cada bloc d’imports, s'ha de deixar una línia en blanc.

## Packages
Els mòduls poden formar packages. Package és una carpeta que conté mòduls i un arxiu \_\_init\_\_.py (pot estar buit).

[http://mikegrouchy.com/blog/2012/05/be-pythonic-__init__py.html](http://mikegrouchy.com/blog/2012/05/be-pythonic-__init__py.html)

    Els packages poden contenir sub-paquets. Els mòduls no han d’estar en un package
    ├── modulo0.py
    └── paquete
        ├── __init__.py
        ├── modulo1.py
        └── subpaquete
            ├── __init__.py
            ├── mod1.py
            └── modulo2.py 


```python
import modulo0
import paquete.modulo1
import paquete.subpaquete.mod1`
```

[https://www.programiz.com/python-programming/package](https://www.programiz.com/python-programming/package)


## Decorators
Per definició, un decorator és una funció que agafa una altra funció i n’**extén** el comportament sense modificar-la explícitament.

Bàsicament un decorator rep una funció com a paràmetre, afegeix alguna funcionalitat i la retorna.

És metaprogramació: part del programa modifica una altra part del programa

!!! example

```python
def my_decorator(f): <br/>
    def wrapper(*args, **kwargs):<br/>
    print("Started")<br/>
    rv = f(*args, **kwargs) #crida a la funció a qui està decorant <br/>
    print("Ended") <br/>
    return rv<br/>
    return wrapper<br/>
    
@my_decorator <br/>
    def my_function(x, y): <br/>
    print(x, y) <br/>
    
my_function("hello", "world")<br/>
```

Quan es crida my_function(...) es fa servir my_decorator, que rep com a paràmetre una funció.

my_decorator afegeix la funcionalitat (abans i/o després) de cridar la funció f(...). En aquest cas, rep la funció my_function.

Si posem els arguments *args, i **kwargs podran fer servir el mateix decorator diferents funcions amb paràmetres diferents.

El mateix decorator es pot fer servir per més d’una funció.

!!! example

```python
def p_decorate(func):<br/>
    def func_wrapper(*args, **kwargs):<br/>
        return "<p\>{0}</p\>".format(func(*args, **kwargs))<br/>
    return func_wrapper<br/>

class Person(object):<br/>
    def __init__(self):<br/>
        self.name = "John" <br/>
        self.family = "Doe"<br/>
   
    @p_decorate <br/>
    def get_fullname(self):<br/>
        return self.name+" "+self.family<br/>

my_person = Person() <br/>
print(my_person.get_fullname())<br/>
```

[https://realpython.com/primer-on-python-decorators/](https://realpython.com/primer-on-python-decorators/)

[http://book.pythontips.com/en/latest/decorators.html](http://book.pythontips.com/en/latest/decorators.html)

[https://youtu.be/tfCz563ebsU](https://youtu.be/tfCz563ebsU)

## Naming conventions

- `package_name`
- `ClassName `
- `method_name `
- `ExceptionName `
- `function_name `
- `GLOBAL_CONSTANT_NAME `
- `global_var_name `
- `instance_var_name `
- `function_parameter_name`
- `local_var_name `

!!! tip

    Les classes s'escriuen en CamelCase<br/>
    Els objectes, mètodes i funcions en snake_case <br/>
    [https://www.python.org/dev/peps/pep-0008/](https://www.python.org/dev/peps/pep-0008/)

### Paraules reservades Python
![img_1.png](img_1.png)
