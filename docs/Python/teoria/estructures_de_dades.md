# Llistes
La llista és una col·lecció ordenada (ordered, no sorted)

- Indiquem entre [ ] els elements separats per comes
- Poden contenir qualsevol tipus de dada (nombres, cadenes, booleans, subllistes...)
- Creació d'una llista

```
>>> llis=[22,True,”una llista”,[1,2]]
```

- Modificació d'una llista

```
>>> llis[0]=[99]
    #ara llis val [[99],True,”una llista”,[1,2]]
>>> llis[0]=99
    #ara llis val [99,True,”una llista”,[1,2]]
```

[https://www.w3schools.com/python/python_ref_list.asp](https://www.w3schools.com/python/python_ref_list.asp)

## Accés als elements

Com hem vist amb els strings

```      
>>> llis = [“a”,”b”,”c”,”d”]
```

[ ]	Slice (L'índex comença a 0 pel principi i a -1 pel final)

```        
>>> element = llis[1]     #element val b 
>>> element = llis[-3] 	  #element val b
```

[:] Range Slice

```
>>> var1 = llis[0:2] 	  # var1 valdrà [“a”,”b”]
>>> var1 = llis[0:4:2] *  # var1 valdrà [“a”,”c”]
```

in 	Membership

```        
‘a’ in llis               >> True
```

Com amb els strings, també tenim els operadors + (concatenació) i * (repetició) (*) 3r paràmetre: quantes posicions salta

## Funcions

| Funcions més comuns | Descripció                  |
|---------------------|-----------------------------|
| len(list)           | Longitud                    |
| max(list)           | Retorna element màxim       |
| min(list)           | Retorna element mínim       |
| list(tuple)         | Converteix una tupla a list |

[https://www.w3schools.com/python/python_ref_functions.asp18](https://www.w3schools.com/python/python_ref_functions.asp18)

## Mètodes

| Mètodes més comuns      | Descripció                                          |
|-------------------------|-----------------------------------------------------|
| list.append(obj)        | Afegeix obj a la llista                             |
| list.insert(index, obj) | Inserta obj a la posició index                      | 
| list.count(obj)         | Compta ocurrències de obj                           |
| list.index(obj)         | Retorna posició de la primera ocurrència de obj     | 
| list.remove(obj)        | Elimina obj de la llista. Primera ocurrencia        |  
| list.reverse()          | Inverteix                                           |  
| list.sort()             | Ordena                                              |


## Comprehensions
Per crear llistes noves:

syntax: llista=\[ expression **for** item **in** list **if** conditional \]

!!! example 

```
>>> squares = [x**2 for x in range(10)]
>>> print squares
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]


>>> [x*2 for x in range(10) if x%2==0]
[0, 4, 8, 12, 16]
```

# Tuples
A diferència d'una llista els seus elements són **immutables**. A canvi són estructures més lleugeres i segures.
Definició:
```
>>> t = (1,2,True,”python”)
>>> type(t)
```
Accedir a un element
```
>>> element = t[1] #element val 2
```
El comportament d’índexs i funcions és igual que a les llistes

# Diccionaris

- Relacionen una clau amb un valor 
- Definició:

```
>>> d={“Nom“: “Eva”, “Edat” :3, “Curs”: “1er”}
```

- Accedir a un element:
 
```
>>> d[“Nom“]
``` 

- Modificar un element:

```    
>>> d[“Nom“]=”Joan”
```

- Com a clau podem fer servir qualsevol valor immutable: nombres, cadenes, booleans o tuples
  
[https://www.w3schools.com/python/python_dictionaries.asp](https://www.w3schools.com/python/python_dictionaries.asp)

| Funcions i mètodes més comuns | Descripció                                                                           |
|-------------------------------|--------------------------------------------------------------------------------------|
| len(dict)                     | num de parells (key, value)                                                          |
| dict.keys()                   | Retorna una llista de claus                                                          |
| dict.values()                 | Retorna la llista de valors                                                          | 
| dict.items()                  | 	Retorna la llista de parells                                                        |
| dict.get(key, default=None)   | Valor associat a key, o valor si no existeix                                         |
| dict.update(dict2)            | Afegeix els parells de dict2 a dict                                                  |
| dict.pop(key)                 | Esborra l’element corresponent a la key                                              |
| dict.del(key)                 | Esborra l’element corresponent a la key o tot el diccionari si no s’especifica key   |
| dict.clear()                  | Esborra el diccionari                                                                |
 
# Set
És una colecció on els seus elements són **immutables i no estan ordenats**. No admet duplicats (s’ignoren)

Definició:

```
>>> s = {1,2,True,”python”}
```

No s’hi pot accedir a través d’un índex o clau, però es pot recòrrer:

```
>>> s = {"apple", "banana", "cherry"} 
>>> for x in s:
>>>    print(x)

>>> “banana” in s
True
```

| Funcions i mètodes més comuns | Descripció                                                             |
|-------------------------------|------------------------------------------------------------------------|
| len(set)                      | 	número d’items del set                                                |
| set.add()                     | Afegir ítems (no es poden modificar però sí afegir)                    |
| set.remove(item)              | 	Esborra l’element corresponent a l’item (error si no existeix)        |
| set.discard(item)             | Esborra l’element corresponent a l’item (no dóna error si no existeix) |
| set.union(set2)               | Uneix dos sets en un tercer set                                        |
| set.update(obj)               | Inserta l’obj en el set                                                |
| set.pop()                     | Esborra l’últim element del set                                        |


!!! abstract "Resum estructures de dades"

    La llista és una col·lecció ordenada i variable. Permet membres duplicats.

    Una Tupla és una col·lecció ordenada i immutable. Permet membres duplicats.

    El set és una col·lecció sense ordenar i sense indexar. No hi ha membres duplicats. No es poden modificar els elements però es poden afegir/eliminar.

    El Diccionari és una col·lecció poc ordenada i canviable. No hi ha membres duplicats. Són variables.

[https://www.w3schools.com/python/python_dictionaries.asp](https://www.w3schools.com/python/python_dictionaries.asp)

!!! example "Resum estructures de dades"

    Llistes: nums = [2, 4, 6, 8, 10]

    Tuples: t = (1, 5, 3, 10, 20, [1,2,3]) # () i ,!!!

    Diccionaris: d = {'a':1, 'b':2, 'c':3} Sets: s = {1,2,3}

[https://clouds.eos.ubc.ca/~phil/courses/eosc582/pdffiles/Python-data-manipulations.pdf](https://clouds.eos.ubc.ca/~phil/courses/eosc582/pdffiles/Python-data-manipulations.pdf)

[https://www.cheatography.com/desmovalvo/cheat-sheets/python3-data-structures/](https://www.cheatography.com/desmovalvo/cheat-sheets/python3-data-structures/)