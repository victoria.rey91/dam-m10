### Strings

- Textos tancats entre cometes simples(‘cadena’) o dobles (“cadena”) . O triples si són multilínia.
- No hi ha tipus caràcter (char). Són tractats com strings de longitud 1.
- Formateig

```python
>>> n = 6
>>> myStr = "Tinc {} anys".format(n)
>>> print(myStr) 
Tinc 6 anys
```

Optimització de formateig: [https://realpython.com/python-f-strings/](https://realpython.com/python-f-strings/)

- Funcions

    * str(x) : converteix x a string
    * len(string) : longitud de string

[https://www.w3schools.com/python/python_ref_string.asp](https://www.w3schools.com/python/python_ref_string.asp)

### Operadors

!!! example

        \>\>\> a = ‘Hello’ <br/>
        \>\>\> b= ‘Python’ <br/>

    | Signe                 | Explicació                                                             | Exemple                  |
    |-----------------------|------------------------------------------------------------------------|--------------------------|
    | +                     | Concatenació                                                           | a + b >> HelloPython     |
    | *                     | Repetition                                                             | a*2 >> HelloHello        |
    | [ ]                   | Slice (L'índex sobre string comença a 0 pel principi i a -1 pel final) | a[1] >> e<br/>a[-1] >> o |
    | [ : ]                 | Range Slice                                                            | a[1:4] >> ell            |
    | in                    | Membership                                                             |  ‘H’ in a >> True        |


### Mètodes

- myStr.count (sub, beg = 0, final = len (str))

    * Compta ocurrències de sub a la cadena o subcadena si informem ppi i fin myStr.isalpha (). Retorna true si la cadena té almenys 1 caràcter i tots els caràcters són alfabètics i fals en cas contrari.

- myStr.isdigit ()

    * Retorna true si la cadena conté només dígits i False d'una altra manera.

- myStr.lower ()
- myStr.upper ()
- myStr.replace (antic, nou)
- myStr.split (str = '')

    * Retorna una llista de strings

- myStr.strip () Trim... myStr.title ()

    * Capitalitza

### Nombres

Enters (int)

``` python
>>> edat=23
>>> type(edat)  .... 
>>> id(edat) …..
>>> edat  ….	# En python no cal definir el tipus de dada!!
```

Reals (float) 	
``` python
>>> pes=2.5 # Tot són objectes. Tenen id, tipus i valor
>>> type(pes)
```

### Nombres (operadors)

| Operador | Nom            | Exemple           | Resultat     |
|----------|----------------|-------------------|--------------|
| +        | suma           | r = 3 + 2         | # r es 5     |
| -        | resta          | r = 4 - 7         | # r es -3    |
| -        | negació        | a = 8<br/>b = - a | # b es - 8   |
| *        | multiplicació  | r = 2*8           | # r es 16    |
| **       | exponent       | r = 2**6          | 	# r es 64   |
| /        | divisió        | r = 3.5 / 2       | # r es 1.75  |
| //       | divisó entera  | r = 3.5 / /2      | 	# r es 1.0  |
| %        | mòdul          | r = 7 % 2         | # r es 1     |

### Nombres (funcions)

| Funció       | Descripció                                                  |
|--------------|-------------------------------------------------------------|
| int (x)      | per convertir x a un enter                                  |
| float (x)    | per convertir x a real                                      |
| abs (x)      | El valor absolut de x                                       |
| pow (x, y)   | El valor de x ** y                                          |
| round(x, y)  | Arrodoniment de x amb y decimals                            |
| divmod(x, y) | Retorna dos valors: resultat de la divisió i el resta       |
| max(,,,...)  | Retorna el valor més gran dels que es passen als arguments  |
| min(,,,...)  | Retorna el valor més petit dels que es passen als arguments |

[https://entrenamiento-python-basico.readthedocs.io/es/latest/leccion5/funciones_integradas.html](https://entrenamiento-python-basico.readthedocs.io/es/latest/leccion5/funciones_integradas.html)