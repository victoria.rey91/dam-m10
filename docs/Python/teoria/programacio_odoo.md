# Programació de mòduls d'Odoo

## Arquitectura

- RAD (Rapid Application Development) Framework escrit en Python
- Integra ORM (Object Relationship Mapping)
  - Els objectes es declaren com a classes de Python que estenen Model que els integra al sistema de persistència, la base de dades. 
  - Veurem que proporciona eines per treballar amb els registres de la BD 
- Interfícies basades en el Model-Vista-Controlador (MVC)

## Estructura d’un mòdul

- Un fitxer \_\_manifest\_\_.py
- Un fitxer \_\_init\_\_.py que importa el codi Python
- Codi python (models, lògica de negoci)
- Fitxers de dades, XML i CSV (dades, views, menus). Que s’han d’indicar al manifest .py
- Capa de presentació (Javascript, CSS)

[https://www.odoo.com/documentation/11.0/reference/module.html](https://www.odoo.com/documentation/11.0/reference/module.html)

### \_\_manifest\_\_.py

``` python
{
    'name': "Title",
    'summary': "Short subtitle phrase", 'description': """Long description""", 'author': "Your name",
    'license': "AGPL-3",
    'website': "http://www.example.com", 'category': 'Uncategorized', 'version': '11.0.1.0.0',
    'depends': ['base'],
    'data': ['[views/]views.xml'],
        ['[views/]obj1.xml'],
        ['[data/]data.xml'],
    'demo': ['[demo/]demo.xml'],
}
```

## Scaffolding

- Podeu generar l’estructura del mòdul creant els directoris i fitxers a mà
- O fent servir l’eina scaffolding que ens proporciona Odoo

!!! note "appexempla"

    appexemple
    ├── init .py
    ├── manifest .py
    ├── controllers
    │ ├── controllers.py
    │ └── init .py
    ├── demo
    │ └── demo.xml
    ├── data
    │ └── data.xml
    ├── models
    │ ├── init .py
    │ └── models.py (vista1.py, vista2.py)
    ├── security
    │ └── ir.model.access.csv
    └── views
    ├── templates.xml
    └── views.xml 

## Estructura de fitxers

- data/: demo i dades xml
- models/: definició de models.
- views/: conté les visualitzacions i les plantilles
- controllers/: conté controladors (rutes HTTP).
- static/: conté els elements web, separats en css/, js/, img/, lib/, …
- security/: conté els csv que dona permisos al mòdul
- i18n/: fitxers de traduccions

## Objectes Odoo: classes Python
```` python
class object_name(models.Model):
_name = 'object.name'
_description = 'Description' [_rec_name = camp1]
[_order = camp1, camp2]........
camp1 = fields.Char(string='Description', required=True) camp2 = fields.Boolean(string='Activa?', default=True) camp4 =
fields.Many2one(
````

- Per cada classe crearà una taula a la base de dades: object_name
- class TodoTask(models.Model): Hereda de models.Model
- Els atributs _xxx defineixen l’objecte/la taula
- La resta d’atributs defineixen els camps de la taula
- Podem posar les classes en un sol fitxer .py o en fitxers separats

```python 
nom_camp = fields.tipus_de_camp(string='Etiqueta', [atributs opcionals])
```
## Definició de camps

- Nom_camp: nom del camp que es crearà a la BD
- String: etiqueta
- Tipus de camp pot ser simple, relacional o funcional
- Exemples:
  - is_done = fields.Boolean(string='Fet?',required=True,help=’Està tot fet?’)
  - date_deadline = fields.Date('Deadline')
  
[https://odoo-new-api-guide-line.readthedocs.io/en/latest/fields.html](https://odoo-new-api-guide-line.readthedocs.io/en/latest/fields.html)
[https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/modelos-estructura-datos-aplicacion.html](https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/modelos-estructura-datos-aplicacion.html)

### Tipus de camps simples:

- Char: strings
- Boolean
- Date
- Selection: llistes de parells valor/descripció
- Integer
- Float
- Monetary
- Text: textos llargs
- Html
- Binary: per imatges, docs
- Datetime

### Possibles atributs dels camps:

- string: l’etiqueta del camp
- size (pels char)
- readonly
- required
- default: valor per defecte
- help: tooltip
- translate: True/False
- index: si cal crear index a la BD
- groups: per restringir accés a certs grups
- states: diccionari amb valors per la UI
- copy: ﬂag per quan es duplica el registre

[https://www.odoo.com/documentation/13.0/howtos/backend.html#common-attributes](https://www.odoo.com/documentation/13.0/howtos/backend.html#common-attributes)

### Atribut “default”

- Mitjançat l'atribut default podrem afegir valors per defecte als camps Sintaxi:
``` python
nom_camp = fields.Tipus(...,default=valor_o_funció)

On:
'nom_camp': Camp sobre el que volem afegir un camp per defecte 'valor_o_funció': Funció que farem servir per calcular el
valor per defecte

Exemple:
active = fields.Boolean(default=True)
start_date = fields.Date(default=fields.Date.today)
```

[https://www.odoo.com/documentation/13.0/howtos/backend.html#default-valu1e1](https://www.odoo.com/documentation/13.0/howtos/backend.html#default-valu1e1)

### Camps especials
A més dels camps que definim nosaltres, per cada classe/taula l’ORM crea els camps:

- id (Id): The unique identifier for a record in its model.
- create_date (Datetime): creation date of the record.
- create_uid (Many2one): user who created the record.
- write_date (Datetime): last modification date of the record.
- write_uid (Many2one): user who last modified the record.
**name**: per defecte Odoo fa servir el camp name per mostrar en les vistes relacionades. Si no tenim camp name, podem definir com a **\_rec\_name** un camp existent. 12

### Tipus de camps relacionals
Ens permet establir relacions entre les dades que estan guardades a diferents taules.
N'hi ha de tres tipus:

- many2one
- one2many
- many2many

[https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/modelos-estructura-datos-aplicacion.html](https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/modelos-estructura-datos-aplicacion.html) 
[https://re-odoo-10.readthedocs.io/capitulos/modelos-estructura-datos-aplicacion/#relaciones-entre-modelos](https://re-odoo-10.readthedocs.io/capitulos/modelos-estructura-datos-aplicacion/#relaciones-entre-modelos](https://re-odoo-10.readthedocs.io/capitulos/modelos-estructura-datos-aplicacion/#relaciones-entre-modelos)

### Camps “related”
La sintaxi d'un camp related és la següent:
`nomCamp: fields.tipus(string='etiquetaNouCamp',related=campPontPropi.campOnAccedir), store=True|False, readonly=True|False)`

On:

- campPontPropi: Camp de l’objecte actual que relaciona amb l’objecte destí
- campOnAccedir: Camp de l’objecte destí que volem obtenir
- store: Si ens interessa o no guardar el valor a l'objecte.
- readonly: Com volem que es mostri el camp

D'aquesta forma modificaríem el model. Haurem de modificar les vistes, per observar el canvi en pantalla.

### Camp relacional (many2one)
many2one: expressa una relació cap a un objecte, usant una **foreign key**
`camp_id = fields.Many2one('objectepare', string="Etiqueta"...`

![img_2.png](img_2.png)

### Camp relacional (one2many)
one2many: expressa una relació d'un objecte cap a molts objectes. (complementària a many2one)
`camp_ids = fields.One2many('objecte_fill', 'camp_invers',string="Etiqueta"...`

![img_3.png](img_3.png)

### Camp relacional (many2many)
many2many: expressa una relació m:n entre 2 objectes
![img_4.png](img_4.png)
inversa - NO ES ^^CREA^^ UN CAMP, SINÓ UNA ^^NOVA TAULA^^
![img_5.png](img_5.png)

## ORM

- L’entorn de desenvolupament d’Odoo ens proporciona l’ORM que suporta els nostres models i els converteix en dades de la base de dades.
- També ens proporciona una API amb mètodes per treballar amb aquests registres de la base de dades.
- Hi ha mètodes per fer les operacions bàsiques CRUD (create, read, update, delete).
- També proporciona decoradors de Python per permetre la interacció dels mètodes que definim amb els objectes de la base de dades:
- **@api.multi**: quan fem servir aquest decorador en un mètode, l’argument self serà el recordset actual. L’haurem de recórrer amb un for.
- **@api.one**: amb aquest decorador, l’argument self tindrà un sol registre.

Els principals són:

- **@api.depends(ﬂd1,...)** es fa servir per camps calculats, per identificar quins canvis haurien de recalcular el camp. Ha de donar un valor als camps calculats o llençar un error.
- **@api.constrains(ﬂd1,...)** es fa servir per funcions de validació: realitza la validació quan els camps mencionats canvien de valor. Si falla la validació, es llença una excepció. Es dispara sobre les operacions CRUD.
- **@api.onchange(ﬂd1,...)** s’utilitza a la IU, per modificar automàticament el valor de camps quan canvia el valor del(s) camp(s) mencionat(s).

[https://hub.packtpub.com/handle-odoo-application-data-with-orm-api-tutorial/](https://hub.packtpub.com/handle-odoo-application-data-with-orm-api-tutorial/)
[https://www.odoo.com/documentation/13.0/reference/orm.html#module-odoo.ap19i](https://www.odoo.com/documentation/13.0/reference/orm.html#module-odoo.ap19i)

- Corresponen a camps els valors dels quals es calculen a partir d’altres camps de la base de dades
- Sintaxi:

``` python
class NomModel(models.Model):
  camp = fields.Tipus('Etiqueta', compute='_funcio')
```

Haurem de definir el mètode a la classe de l’objecte:

``` python
@api.multi
@api.depends('objectes necessaris') 
def _funcio(self):
  self.atribut = self.atribut-de-l-objecte // per exemple….
```

!!! example

![img_6.png](img_6.png)
![img_7.png](img_7.png)

https://www.odoo.com/documentation/13.0/howtos/backend.html#computed-fields-and-default-values https://odooforbeginnersblog.wordpress.com/2017/03/01/how-to-override-an-api-depends-decorated-method/
https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/modelos-estructura-datos-aplicacion.html#campos-calculados

## Restriccions

Es poden definir restriccions des de SQL i des de Python.

Les **restriccions SQL** es defineixen a l’atribut `\_sql\_constraints` del model.  
Espera una llista de tuples (nom, check_sql, missatge_error).

- `nom` ha de ser un nom vàlid per una restricció SQL
- `check_sql` ha de ser una restricció vàlida sobre la taula Postgres corresponent.

``` python
_sql_constraints = [
  ('name_description_check', 
  'CHECK(name != description)',
  "The title of the course should not be the description"),
]
```
https://www.odoo.com/documentation/13.0/howtos/backend.html#model-constraints https://re-odoo-10.readthedocs.io/capitulos/modelos-estructura-datos-aplicacion/#restricciones-del-modelo

Les **restriccions Python** es defineixen en funcions amb el decorator **@api.constrains**('nom_camp').

- nom_camp: camp sobre el qual aplica la restricció. Es disparà quan es modifiqui.
- la funció s’aplica sobre el resultset. Haurem de fer un for per recòrrer’l
- la funció llança una excepció si no s’acompleix la restricció

![img_8.png](img_8.png)

## Aspectes a tenir en compte

- Cada cop que modifiquem un mòdul, haurem d’actualitzar-lo.
- Si a més hem tocat codi Python, haurem de fer un restart.
- Els canvis que realitzem sobre les vistes en mode depuració no queden reflectits en els fitxers xml del mòdul.
- Ara bé, si fem algun canvi que “trenca” la vista i no ho podem arreglar des del mode depuració, la solució implica fer un upgrade del mòdul.

## Menús, accions i vistes
La interacció amb l'usuari a Odoo es programa a través de fitxers XML  
En aquests fitxers definirem 3 tipus d’elements:

- Menús
- Accions
- Vistes
- (i Dades)

https://www.odoo.com/documentation/11.0/reference/views.html#OdooDevelopmentEssentials.pdf Cap.6

``` xml
<?xml version="1.0"?>
<odoo>
  <!-- To-Do Task Form view -->
  <record id="view_form_todo_task" model="ir.ui.view">
  </record>

  <!-- Definició d’una acció -->
  <act_window id="action_todo_task"
  .../>

  <!-- Definició d’un menú -->
  <menuitem id="menu_todo_task"
  .../>
</odoo>
```

## Esquema del/dels fitxer .xml
A través dels fitxers XML definim registres a la base de dades
`<record id="view_form_todo_task" model="ir.ui.view">`
generarà un registre a la taula `ir.ui.view`  
`<act_window>`  
`<menuitem>`
Són shortcuts per afegir registres directament a les taules
`ir.actions.act_window` i `ir.ui.menu`

``` xml
<menuitem id="menu_todo_task" 
  name="Tasques a fer" 
  action="action_todo_task"
  parent="menu_todo_task_top" 
  sequence="i"
/>
```

On:
`id`: identificador únic del menú
`name`: etiqueta que es mostrarà com a nom del menú
`action`: identificador de l’acció que s’executa si l’usuari prem el menú
`parent`: identificador del menú pare. Si no és menú principal

Existeixen tres tipus d'accions

- Window: obre una vista
- Report: imprimeix un informe
- Wizard: executa un assistent per fer un determinat treball o procés

Les accions de tipus Window són les més habituals

``` xml
<act_window id="action_todo_task" 
  name="Tasques" 
  res_model="todo.task"
  view_mode="tree,form" (calendar,graph,pivot,tree,form...) 
  domain="[('camp','criteri',valor)]"

/>
```

On:

`id`: identificador únic de l’acció
`name`: nom de l’acció
`res_model`: nom de l’objecte d’Odoo sobre el qual opera l’acció
`view_mode`: llista de vistes a usar

En el view_mode s’indiquen, en ordre les vistes a utilitzar.  

Es pot triar entre

- form (Formulari)
- tree (Llista)
- calendar (Calendari)
- graph (Gràfic)
- gantt (Diagrama de Gantt)
- search (Afegir filtres per la cerca)
- ...
https://www.odoo.com/documentation/13.0/reference/views.html 

### Views - fitxer xml

``` xml
<record id="view_form_todo_task_ui" model="ir.ui.view">
  <field name="model">todo.task</field>
  <field name="priority">15</field>
  <field name="arch" type="xml">
  
  <form> / <tree> / <graph> / <search> ...
  …
  </form>
</record>
```
https://www.odoo.com/documentation/13.0/reference/views.html#list
https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/vistas-disenar-la-interfa z.html#vistas-de-lista

``` xml
<tree default_order="camp1,camp2 desc">
  <field name="name"/>
  <field name="user_id" class="oe_inline" />
  <field name="is_done" />
</tree>
```

En aquest cas tindríem una llista de visualització de resultats senzilla amb aquests 3 camps.

``` xml
<record id="view_filter_todo_task" model="ir.ui.view">
  <field name="name">To-do Task Filter</field>
  <field name="model">todo.task</field>
  <field name="arch" type="xml">
    <search>
      <field name="user_id" filter_domain=.	/>
      <filter string="Not Done" name="filter_not_done" domain="[('is_done','=',False)]"/>
      <filter string="Done" domain="[('is_done','!=',False)]"/>
    </search>
  </field>
</record>
```

Els filtres per buscar es mostren a les pàgines de llistar resultats. <tree>
<field>: camp sobre els que buscar
<filter>: filtres predefinits 

https://www.odoo.com/documentation/13.0/reference/views.html#search https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/vistas-disenar-la-interfaz.html#vistas-disenar-la-interfaz
Google Books:

``` xml
<form>
  <header>
    <!-- To add buttons and status widget → Exemple de formulari de presentació d’un registre -->
  </header>
  <sheet>
  <div class="xxx">
    <h3>
    <label for="user_id" class="oe_edit_only"/>
    <field name="user_id" class="oe_inline" />
     </h3>
  </div>
  
  <group name="group_top">
  <group name="group_left">
    <field name="date_deadline" attrs="{'invisible': [('state', '=', 'draft')]}" />
    <field name="refers_to" />
    <field name="is_done" />
    .....

</sheet>
```

https://www.odoo.com/documentation/13.0/reference/views.html#form	35
`<sheet>`
layout que simula un full dins el <form>

`<group>`
Per agrupar camps. Dins d’un <group>, el tag <field> també mostra etiqueta. Els camps que mostrem fora de group, si volem posar etiqueta, l’hem d’explicitar amb el tag <label for=”nomdelfield”>

`<notebook>`
Layout que mostra una sèrie de pestanyes per organitzar el contingut com un quadern:

`<page string="titolPestanya">`

https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/vistas-disenar-la-interfaz.html#vistas-de-formulario

### Funcions onChange()

- Els camps del formulari poden tenir associades funcions onchange.
- Aquestes funcions fan servir el decorator @api.onchange('nom_camp') per saber sobre quin camp s’aplicaran.
- S’apliquen quan es modifica el valor del camp al formulari. No a la base de dades. Pot servir per informar valors d’altres camps abans d’enviar-los a la base de dades.
- O per mostrar un missatge a l’usuari si ha introduït un valor incorrecte
https://www.odoo.com/documentation/13.0/howtos/backend.html#onchange https://odooforbeginnersblog.wordpress.com/2017/02/24/how-onchange-function-works-api-onchange https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/vistas-disenar-la-interfaz.html#eventos-on-change 37
![img_9.png](img_9.png)

## Càrrega de dades

- Ens pot ser útil en dos aspectes
    - Migracions de dades en la substitució d'un sistema informàtic
    - Càrrega de dades de demostració
- Podrem fer la càrrega mitjançant
    - Fitxers XML
    - Fitxers CSV
- Les dades de demostració només es carreguen en el procés d’instal·lació del mòdul. No quan fem un upgrade
- Els fitxers a carregar s'han de declarar en el fitxer manifest .py
  
https://www.odoo.com/documentation/13.0/reference/data.html

## Càrrega de dades XML

Ens permet carregar dades de diferents objectes. Caldrà seguir un model com el que teniu a continuació:

``` xml
<?xml version="1.0" encoding="utf-8"?>
<odoo>
<data>
<record id="idRecurs" model="nom.objecte">
  <field name="camp1">valor</field>
  <field name="camp2">valor</field>
  ...
</record>
<record id="idRecurs" model="nom.objecte">
  <field name="camp1">valor</field>
  ...
</record>
....
</data>
</odoo>
```

Si volem carregar camps ^^one to many^^ o ^^many to many^^:
`<field name="tag_ids" eval="[(6,0,[ref('vehicle_tag_leasing'),ref('fleet.vehicle_tag_compact'),ref('flee t.vehicle_tag_senior')] )]" />`

S’ha de fer servir una llista de tuples. Cada tupla és un comandament que fa accions diferents en funció del codi utilitzat:

- (0,_,{'field':value}): crea un nou registre i el vincula a aquest
- (1,id,{'field':value}): actualitza els valors d’un registre ja vinculat
- (2,id,_): desvincula i elimina un registre relacionat
- (3,id,_): desvincula però no elimina un registre relacionat
- (4,id,_): vincula un registre ja existent
- (5,_,_): desvincula però no elimina tots els registres vinculats
- (6,_,[ids]): substitueix la llista de registres vinculats amb la llista proporcionada

[https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/serializacion-de-datos-y-datos-del-modulo.html#configuracion-de-los-valores-de-los-campos-de-relacion](https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/serializacion-de-datos-y-datos-del-modulo.html#configuracion-de-los-valores-de-los-campos-de-relacion)

El “_” representa valors irrellevants, generalment omplert amb 0 o False. 

### Càrrega de dades CSV

- Només permet la introducció de dades d'un mateix objecte
- El nom del fitxer serà el nom de l'objecte en el qual volem afegir els registres
- En la primera línia cal indicar el nom dels camps que volem introduir
- Exemple:
``` xml
id,name,user_id/id,date_deadline
todo_task_a,"Installar un altre cop Odoo","base.user_root","2018-11-11" todo_task_b,"Crear una base de dades nova","
base.user_root",""
```
Per Importar/Exportar:
[https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/serializacion-de-datos-y-datos-del-modulo.html#exportar-e-importar-datos](https://fundamentos-de-desarrollo-en-odoo.readthedocs.io/capitulos/serializacion-de-datos-y-datos-del-modulo.html#exportar-e-importar-datos)

### Fitxer de permisos de mòdul
Per definir els permisos d’accés a un mòdul es fa servir el fitxer **/security/ir.model.access.csv**, que s’ha d’incloure en el manifest, amb el format:
![img_10.png](img_10.png)
on:
- `id`: identificador del registre de permisos
- `name`: nom del model
- `model_id`: consta del literal “model_” + el nom del model
- `grup d’usuaris`: si el deixem en blanc estarem fent públic per tots els grups
- els 4 darrers valors són els accessos permesos: lectura, escriptura, creació i link.

https://www.odoo.com/documentation/13.0/reference/security.html#reference-security-acl  