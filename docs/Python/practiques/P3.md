# Mòdul propi: Hospital

## Objectius
Desenvolupar components per a un sistema ERP-CRM analitzant i utilitzant el llenguatge de programació incorporat.  
Adaptar sistemes ERP-CRM identificant requeriments d'un supòsit empresarial.

## Què s’ha d’entregar

1. El mòdul (mantenint l'estructura de fitxers) en un fitxer comprimit (zip) anomenat CogNomM10UF2H1.zip
2. Fitxer pdf amb captures de pantalla on es vegin els menús i formularis desenvolupats. A totes les captures s’han de veure les vostres inicials.

## Activitat
Es vol gestionar tot el que fa a la gestió de pacients d'un hospital des d’Odoo. Per fer-ho, desenvoluparem un mòdul anomenat HospitalXYZ, i, com a primer pas, farem la gestió dels pacients. (XYZ seran les vostres inicials)

Has de crear un mòdul anomenat hospitalxyz, que contingui:

- L'objecte pacient, amb les dades següents:

| Etiqueta          | Tipus     | Required | Default |
|-------------------|-----------|----------|---------|
| Nom               | Char      | Yes      |         |
| Cognom            | Char      | Yes      |         |
| DNI               | Char      | Yes      |         |
| Gènere            | Selection | No       |         |
| Data de naixement | Date      | Yes      |         |
| Nacionalitat      | Many2One  | Yes      |         |
| Ingrés?           | Boolean   | Yes      |         |
| Data d'ingrés     | Date      | No       |         |
| Informació        | Text      | No       |         |
| Assegurança       | Char      | No       |         |

!!! info

    Cada camp haurà de mostrar un text d'ajuda.  
    Aquests camps es guardaran en una taula anomenada hospitalxyz_pacient, on xyz són les vostres inicials.

- Un element de menú a la part superior anomenat HospitalXYZ.

![img_2.png](img_2.png)

- Un element de menú a la part esquerra que depengui d'HospitalXYZ, anomenat Gestió de pacients.

- Un altre element de menú, fill de Gestió de pacients, anomenat Llista de Pacients.

![img_8.png](img_8.png)

- Una vista en arbre bàsica, visualitzada a partir de l'element de menú Llista de Pacients amb la següent informació:
  
    - Nom
    - Cognom
    - Ingrés

![img_9.png](img_9.png)

- La vista formulari associada a l'objecte Pacient.
- Una vista de tipus search que permeti cercar per Nom i DNI.

![img_10.png](img_10.png)

- Un fitxer .xml que carregui les dades (totes) de 3 pacients.
- Una icona pel mòdul

![img_6.png](img_6.png)

## Referències  
[https://www.odoo.com/documentation/13.0/howtos/backend.html](https://www.odoo.com/documentation/13.0/howtos/backend.html)  
[https://www.odoo.com/documentation/13.0/reference/guidelines.html](https://www.odoo.com/documentation/13.0/reference/guidelines.html)  
[https://www.odoo.com/documentation/13.0/reference/module.html](https://www.odoo.com/documentation/13.0/reference/module.html)  
[https://www.odoo.com/documentation/13.0/reference/data.html](https://www.odoo.com/documentation/13.0/reference/data.html)  
[https://www.odoo.com/documentation/13.0/reference/views.html](https://www.odoo.com/documentation/13.0/reference/views.html)   
