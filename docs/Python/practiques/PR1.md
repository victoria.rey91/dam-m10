# Exercicis Introducció a Python

## Tipus bàsics

- **1.** Escriu un programa que demani dos nombres i que retorni la seva **mitjana aritmètica**.
- **2.** Escriu un programa que demani una **distància** en peus i polzades i que escrigui aquesta distància en centímetres. Recorda que un peu son 12 polsades i una polzada són 2.54 cm
- **3.** Escriu un programa que demani una **temperatura** en graus Celsius i que escrigui la temperatura en graus Fahrenheit. Recorda que la relació entre graus Celsius (C) i Fahrenheit (F) és la següent F – 32 = 1,8 * C
- **4.** Escriu un programa que demani una quantitat de **segons** i que escrigui quants minuts i segons són.

## Exercicis de manipulacions de col·leccions

- **5.** Escriu un programa que creï una **llista amb les vocals**.

    - Que demani a l'usuari quin número d’element de la llista vol modificar i amb quin valor, i mostri per pantalla la llista modificada.
    - Després que demani quina lletra vol modificar i amb quin valor, i mostri per pantalla la llista modificada.

- **6.** Escriu un programa que mostri la taula de multiplicar del 7 a partir d'una llista. Cal que definiu una llista en la que es guardin els valors de la taula de multiplicar.
- **7.** Escriu un programa amb un diccionari que relacioni el nom de 4 alumnes i la nota que han obtingut en un exercici.

    - Que demani a l’usuari de quin alumne vol saber la nota, i que mostri el resultat per pantalla.

- **8.** Escriu un programa en el que donades dues llistes de paraules (definides internament en el programa i sense repeticions) escrigui les següents llistes (sense repeticions)

    - Llista de paraules que apareixen tant en la primera llista com en la segona (intersecció)
    - Llista de paraules que apareixen en la primera llista, però no en la segona (diferència)
    - Llista de paraules que apareixen en la segona llista, però no en la primera 
    - Llista de paraules que apareixen en alguna de las dues llistes (unió)

## Exercicis de sentències condicionals

- **9.** Escriu un programa que demani dos nombres enters i que calculi la seva divisió, escrivint si la divisió és exacta o no. Has de controlar el cas que indiqui que no es pot dividir per 0.
- **10.** Escriu un programa que pregunti primer si es vol calcular l'àrea d'un triangle o la d'un cercle. Si es contesta que es vol calcular l'àrea d'un triangle, el programa ha de demanar aleshores la base i l'alçada i escriure l'àrea. Si es contesta que es vol calcular l'àrea del cercle, el programa aleshores ha de demanar el radi i escriure l'àrea.
- **11.** Escriu un programa per repartir una quantitat d'euros en el nombre de bitllets i monedes corresponents, tenint en compte un algoritme que ens asseguri el menor nombre de bitllets/monedes (és a dir, sempre que puguem utilitzar un bitllet de 10€, no utilitzar-ne dos de 5€ ni cinc monedes de 2€)
    
    - Suposa que no existeixen els bitllets majors de 50 € i que les quantitats introduïdes són enteres.

- **12.** Escriu un programa que llegeixi una data (dia, mes i any), determini si la data correspon a un valor vàlid i ho escrigui. Les dates seran tres dades de tipus enter que correspondran a un dia, un mes i un any (dd, mm, aa).
    
    - Els mesos 1, 3, 5, 7, 8, 10 i 12 tenen 31 dies.
    - Els mesos 4, 6, 9 i 11 tenen 30 dies.
    - El mes 2 té 28 dies, excepte quan l'any és de traspàs, que té 29 dies.
    - Són de traspàs els anys que són múltiples de 400 i els anys que són múltiples de 4 però no de 100.

## Exercicis sobre bucles

- **13.** Escriu un programa que demani dos nombres enters i escrigui la suma de tots els enters des del primer nombre fins al segon. Comprova que el segon nombre és més gran que el primer
- **14.** Escriu un programa que demani un nombre enter major que 0 i que calculi el seu **factorial**
- **15.** Escriu un programa que mostri els múltiples de 7 que hi ha entre els números del 0 al 100. Fes-ho amb una estructura for.
- **16.** Escriu un programa que permeti crear una llista de paraules. El programa haurà de demanar un nombre i a continuació sol·licitar tantes paraules com hagi indicat l'usuari. Finalment mostrarà la llista creada.
- **17.** Escriu un programa que permeti crear dues llistes de paraules i que, a continuació, elimini de la primera llista les paraules de la segona llista.
- **18.** Escriu un programa que donat un número comprovi si és **primer**. (for/else)
- **19.** Escriu els nombres enters positius narcisistes de tres xifres. Els nombres narcisistes de tres xifres són de la forma: $abc=a^3+b^3+c^3$ .
    
El resultat hauria de mostrar els números 153, 370, 371 i 407, ja què:

$$
\begin{align}
    153 = 1^3 + 5^3 + 3^3\\
    370 = 3^3 + 7^3 + 0^3\\
    371 = 3^3 + 7^3 + 1^3\\
    401 = 4^3 + 0^3 + 7^3\\
\end{align}
$$

- **20.** Donats dos nombres enters n1 i n2 amb n1 < n2, escriu **per pantalla tots els nombres enters** dins l’interval [n1, n2] en ordre creixent.
- **21.** Donats dos nombre enters n1 i n2 amb n1 < n2 , escriu tots els nombres enters dins l’interval [n1, n2] en ordre decreixent.
- **22.** Donats dos nombre enters n1 i n2 amb n1 < n2, escriu les **arrels quadrades dels nombres enters** dins l'interval [n1, n2] en ordre creixent.
- **23.** Donats dos nombre enters n1 i n2 amb n1 < n2, escriu els nombres enters parells que hi ha dins l’interval [n1, n2] en ordre creixent. El nombre zero es considera parell.

## Exercicis amb bucle i comprehension

- **24.** Escriure els 10 primers múltiples de 2
- **25.** Escriure els n primers múltiples de m

## Exercicis de cadenes

- **26.** Escriu un programa que donada una paraula ens digui si és “alfabètica”. Una paraula és “alfabètica” si totes les seves lletres estan ordenades alfabèticament.
    
    Per exemple, “amor“, “ceps“ e “himno“ són paraules “alfabètiques”.

- **27.** Escriu un programa que donada una cadena ens digui si és un palíndrom o no. Una frase o cadena és palíndroma si es pot llegir en ordre o a l'inrevés obtenint la mateixa frase, per exemple: "dabale arroz a la zorra el abad", és palíndroma.

- **28.** Escriu un programa que donat un DNI, el verifiqui comprovant que la lletra és correcta. L’última lletra del DNI es pot calcular a partir dels seus números. Per a això només has de dividir el nombre per 23 i quedar-te amb la resta. La resta és un nombre entre 0 i 22. La lletra que correspon a cada nombre la tens en aquesta taula:

|                                                             |
|-------------------------------------------------------------|
| 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22  |
| T R W A G M I F P D X B N J Z S Q V H L C K E               |

!!! note

    Una implementació basada en prendre una decisions amb if -elif condueix a un programa “molt” llarg. Si fas servir l'operador d'indexació de cadenes, el programa tot just ocupa 3 línies

## Exercicis de fitxers

- **29.** Escriu un programa que donat un fitxer de text d'entrada, el llegeix i mostra la llista de paraules ordenades alfabèticament. Les paraules no poden estar repetides, i no han de ser keysensitive. És a dir, a la llista només hauria d’aparèixer un cop la paraula “hola” si en el fitxer posés “Hola, hola, hoLA ...”

## Exercicis extra

- **30**. Escriu un programa que donada una paraula escrigui per pantalla tots els seus **anagrames**. Un anagrama és el resultat de construir una nova paraula a partir de la reordenació de les lletres d’una paraula.

- **31.** La criba d’**Eratóstenes**. És un mètode per trobar els nombres primers menors que un número donat.
    
Com funciona aquest mètode? S'escriu una llista A amb els nombres des del 2 fins a l'enter més gran que es vulgui avaluar.

  - El primer nombre de la llista és un nombre primer.
  - Es tatxen de la llista A els múltiples del primer nombre (atenció, el primer nombre no).
  - Es fa el mateix amb el següent nombre de la llista A que no estigui tatxat, i així successivament fins que arribem a la meitat de la llista.
  - Els nombres no tatxats de la llista A són tots els nombres primers cercats.

La manera que tindrem de “tatxar” un número de la llista serà assignant-li a aquella posició el número 0.

Exemple amb el número 12:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Llista inicial  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2, 3, 4, 5, 6, 7, 8, 9, 10, 11]  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tatxo els múltiples de 2  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 6 8 10  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2, 3, 0, 5, 0, 7, 0, 9, 0, 11]  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tatxo els múltiples de 3  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6 9  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2, 3, 0, 5, 0, 7, 0, 0, 0, 11]  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tatxo els múltiples de 5  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2, 3, 0, 5, 0, 7, 0, 0, 0, 11]  

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tatxo els múltiples de 7  
  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2, 3, 0, 5, 0, 7, 0, 0, 0, 11]   

- **32.** La seqüència de Recaman.

    - **a. Mode texte:**
        Escriu un programa que calculi la seqüència de Recaman  
        [https://solmos.netlify.com/es/post/2019-02-20-la-secuencia-de-recaman/la-secuencia-de-recaman/](https://solmos.netlify.com/es/post/2019-02-20-la-secuencia-de-recaman/la-secuencia-de-recaman/)  
        0,1,3,6,2,7,13,20,12,21,11,22,…  

    - **b. Ampliació:** Amb el mòdul **“turtle”** de Python, escriu un programa que mostri la representació gràfica de la seqüència de Recaman.
    A F32 necessitaràs instal·lar tkinter:
    ```
    sudo apt-get install python-tk 
    ```
    
    Mètodes de mòdul “turtle”: [https://docs.python.org/3/library/turtle.html](https://docs.python.org/3/library/turtle.html)

![PR1-1.png](../../src/images/PR1-1.png)

- **33.** Mòduls **urllib/requests i Beautiful Soup.**
    
    Escriu un programa que es connecti a la pàgina web [https://isitchristmas.com/](https://isitchristmas.com/) i parsegi el contingut de la pàgina per saber si avui és el dia de Nadal.    
    Podeu fer servir els mòduls **urllib** o **requests**.      
    Podeu parsejar la pàgina “a mà” o podeu fer servir el mòdul **BeautifulSoup**…

- **34.** Gràfics.
    Escriu un programa que donades dues llistes amb el temps emprat i les notes obtingudes en un examen mostri les notes en una gràfica per veure la relació entre la nota i el temps emprat.      
    Mostreu també la recta de regressió

![PR1-2.png](../../src/images/PR1-2.png)