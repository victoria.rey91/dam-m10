## Exercicis: Aplicació de gestió de clients

Cal construir una aplicació per gestionar la informació d'un grup de clients. Les dades es guardaran en una llista.
El codi quedarà organitzat en 4 mòduls:

- menu.py
- client.py
- llibreta.py
- aplicacio.py

_Els mòduls i l’estructura de l’aplicació proposada són orientatives._

### Mòdul menu.py
Aquest mòdul incorpora dos mètodes per imprimir els menús per pantalla

Mètodes

- **mostrar_menu_principal** mostra per pantalla les opcions del menú principal, i retorna l'opció seleccionada per l'usuari
- **mostrar_menu_consulta** mostra per pantalla  les opcions del menú consulta i retorna l'opció seleccionada per l'usuari

![img.png](img.png)

![img_1.png](img_1.png)

### Mòdul client.py
Defineix la classe **Client**

Atributs

- identificador (únic per cada client), nom, cognom, telefon, correu, adreca, ciutat

Mètodes

- constructor amb tots els paràmetres per ser creada
- \_\_str\_\_ retorna una representació de tipus string del client

### Mòdul llibreta.py
Defineix la classe **Llibreta**
Atributs

- llista_clients de tipus llista
- id_client comptador per generar l'identificador de cada client

Mètodes

- **constructor** inicialitza la llistaClients i idClient
- **get_llista_clients** mostra per pantalla tots els clients de la llista
- **afegir_client** rep com a paràmetres les dades del client, calcula l'identificador, crea un nou Client i l'afegeix a la llista
- **eliminar_client** rep com a paràmetre l'identificador del client i l'elimina de la llista
- **cercar_per_id** rep com a paràmetre l'identificador del client. Retorna una llista amb el client que ha trobat en la primera posició
- **cercar_per_nom** rep com a paràmetre el nom del client. Retorna una llista amb els clients que ha trobat.
- **cercar_per_cognom** rep com a paràmetre el cognom del client. Retorna una llista amb els clients que ha trobat.

### Mòdul aplicacio.py
Crea els objectes de tipus Menu i LlibretaClients

Mostra el menú principal
En funció de l'opció

- Demana les dades i afegeix el client
- Mostra la llista de clients, demana l'identificador i elimina el client
- Mostra el menú Consulta i recupera l'opció demanada
- En funció de l'opció demana les dades necessàries, crida els mètodes corresponents i mostra la informació
- Surt

**TIPS ordenació:**
[https://docs.python.org/es/3/howto/sorting.html](https://docs.python.org/es/3/howto/sorting.html)

Apartats: _Funciones clave i Funciones del módulo operator_
