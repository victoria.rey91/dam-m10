# Primera part

**1.** Calcular el promedio de una lista de números.

Solución:

```python

def calcular_promedio(lista):
suma = sum(lista)
promedio = suma / len(lista)
return promedio

numeros = [2, 5, 8, 10, 12]
resultado = calcular_promedio(numeros)

print("El promedio de la lista es:", resultado)
```
**2.**Determinar si un número es primo o no.

Solución:

```python

def es_primo(numero):
if numero < 2:
return False
for i in range(2, int(numero/2) + 1):
if numero % i == 0:
return False
return True

numero = 17
if es_primo(numero):
print(numero, "es un número primo.")
else:
print(numero, "no es un número primo.")
```
**3.**Convertir una cadena de caracteres a minúsculas.

Solución:

```python

cadena = "Hola Mundo"
cadena_minusculas = cadena.lower()
print("La cadena en minúsculas es:", cadena_minusculas)
```
**4.**Calcular el factorial de un número.

Solución:

```python

def calcular_factorial(numero):
factorial = 1
for i in range(1, numero + 1):
factorial *= i
return factorial

numero = 5
resultado = calcular_factorial(numero)
print("El factorial de", numero, "es:", resultado)
```
**5.**Verificar si una palabra es un palíndromo.

Solución:

```python

palabra = "reconocer"
if palabra == palabra[::-1]:
print("La palabra", palabra, "es un palíndromo.")
else:
print("La palabra", palabra, "no es un palíndromo.")
```
**6.**Contar la cantidad de vocales en una cadena de caracteres.

Solución:

```python

def contar_vocales(cadena):
vocales = "aeiouAEIOU"
cantidad_vocales = 0
for caracter in cadena:
if caracter in vocales:
cantidad_vocales += 1
return cantidad_vocales

cadena = "Hola mundo"
resultado = contar_vocales(cadena)
print("La cantidad de vocales en la cadena es:", resultado)
```
**7.**Generar los primeros n números de la serie de Fibonacci.

Solución:

```python

def generar_fibonacci(n):
fibonacci = [0, 1]
for i in range(2, n):
siguiente_numero = fibonacci[i - 1] + fibonacci[i - 2]
fibonacci.append(siguiente_numero)
return fibonacci

n = 10
resultado = generar_fibonacci(n)
print("Los primeros", n, "números de la serie de Fibonacci son:", resultado)
```
**8.**Calcular el área y perímetro de un círculo dado su radio.

Solución:

```python

import math

def calcular_area_perimetro_circulo(radio):
area = math.pi * radio**2
perimetro = 2 * math.pi * radio
return area, perimetro

radio = 5
area, perimetro = calcular_area_per
```

**9.** Ordenar una lista de números de forma ascendente o descendente.

Solución:
```python
def ordenar_lista_numeros(lista, ascendente=True):
    lista_ordenada = sorted(lista, reverse=not ascendente)
    return lista_ordenada

numeros = [7, 2, 9, 1, 5]
orden_ascendente = ordenar_lista_numeros(numeros)
orden_descendente = ordenar_lista_numeros(numeros, ascendente=False)

print("Lista original:", numeros)
print("Lista ordenada de forma ascendente:", orden_ascendente)
print("Lista ordenada de forma descendente:", orden_descendente)
```

**10.** Validar si un año es bisiesto o no.

Solución:

```python

def es_bisiesto(anio):
if (anio % 4 == 0 and anio % 100 != 0) or (anio % 400 == 0):
return True
else:
return False

anio = 2024
if es_bisiesto(anio):
print(anio, "es un año bisiesto.")
else:
print(anio, "no es un año bisiesto.")
```