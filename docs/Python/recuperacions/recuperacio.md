# Primera part


1. Calcular la mitjana d'una llista de números. 
2. Determinar si un número és primer o no. 
3. Convertir una cadena de caràcters a minúscules. 
4. Calcular el factorial d'un número. 
5. Verificar si una paraula és un palíndrom. 
6. Comptar la quantitat de vocals en una cadena de caràcters. 
7. Generar els primers n números de la sèrie de Fibonacci. 
8. Calcular l'àrea i perímetre d'un cercle donat el seu radi. 
9. Ordenar una llista de números de forma ascendent o descendent. 
10. Validar si un any és de traspàs o no.

!!! note

    Heu de programar un mínim de **dos** exercicis i explicar-me com estan fets.

# Segona part

- Afegeix a la Llista de Pacients el camps "Asegurança"
- Filtrar per nom
- Canviar el text de "Gestió de Pacients" -> "Administració de Pacients" 
- Càrrega un nou usuari

!!! note

    Executeu **dos** canvis del proposats al modul de l'hospital i expliqueu-me el procés.


